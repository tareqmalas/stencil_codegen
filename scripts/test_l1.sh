for z in {1..18}
do 
    for o in -4 0 4
    do
        dk=$((16*$z+$o))
        make MUTANT_KERNEL=bagm_mute2x2 DRIVER_OPTS=" -DUSE_FORTRAN_KERNEL=0 -DVERIFY=1 -DN_TESTS=10 -DN_I=4 -DN_J=4 -DN_K=$dk -DTOUCH_L1=0 -DPRINT_TIME_DETAILS=1" BUILDER_OPTS="-v 0 -i 1" -B 2>&1 | tee -a test_l1.log
        mpirun -n 1 ./build/bagm_mute2x2 2>&1 | tee -a test_l1.log
    done
done
