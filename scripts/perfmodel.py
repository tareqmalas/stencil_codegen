#!/usr/bin/env python3

import itertools
from collections import namedtuple

Jam = namedtuple('Jam', 'i j')

class Kernel:
    def __init__(self, prologue, inner_iter, epilogue=0, nregisters=19, stencil_name='', jam=(1,1), k_repeats=1, **ignored):
        self.ni = 4     # these are now unified for all the in-L1 experiments
        self.nj = 14
        self.k_repeats = k_repeats
        jam = Jam(*jam)
        args = locals(); del args['self']
        for name in args: setattr(self,name,args[name])
        self.njams = jam.i * jam.j
    def cycles_per_kstrip(self, k):
        if 'mute' in self.stencil_name:
            chunk_length = 2                                # 1 packed registers
        elif 'lc' in self.stencil_name:
            chunk_length = 4                                # 2 packed registers
        else:
            raise Exception('unknown kernel type')
        inner_k = (k-2)/chunk_length    # none of the used kernels compute stencils outside the loop
#        print("stencil name prologue epilogue inner_iter inner_k")
#        print(self.stencil_name, self.prologue, self.epilogue, self.inner_iter, inner_k)
        return self.prologue + self.epilogue + self.inner_iter * inner_k/self.k_repeats
    def cycles_per_stencil(self, k):
        save_restore = 19 * (4 + 2)          # always save/restore 19 registers, 4 cycles to save, 2 to restore
        stencils_per_kstrip = (k-2) * self.njams
        n_stencils = (k-2)*(self.ni-2)*(self.nj-2)
        return save_restore/n_stencils + self.cycles_per_kstrip(k)/stencils_per_kstrip
    def mstencils(self, k):
        mcycles_per_second = 850.    # hardware
        return mcycles_per_second / self.cycles_per_stencil(k)

mute_2x2_27pt = Kernel(prologue=118, epilogue=96, inner_iter=232, stencil_name='bagm_mute_27pt', jam=Jam(2,2)) # An example

def make_table(args):
    '''
    ~/src/sparse_mutant$ scripts/perfmodel.py --table
      bagm_mute_27pt_1x1                20.917226 Mstencil/s            40.6 cycles/stencil
      bagm_mute_27pt_1x2                41.300613 Mstencil/s            20.6 cycles/stencil
      bagm_mute_27pt_1x3                61.170342 Mstencil/s            13.9 cycles/stencil
      bagm_mute_27pt_2x2                80.584151 Mstencil/s            10.5 cycles/stencil
      bagm_mute_27pt_2x3                117.994859 Mstencil/s           7.2 cycles/stencil
       bagm_mute_7pt_2x2                221.593153 Mstencil/s           3.8 cycles/stencil
       bagm_mute_7pt_2x3                302.335329 Mstencil/s           2.8 cycles/stencil
      bagm_lc2x3_7pt_2x3                184.708249 Mstencil/s           4.6 cycles/stencil

    Note: These counts are not accurate.  Somehow the cycle counts
    coming out of the simulator are all incorrect now.  It appears that
    this occurred when the number of unrolls was made a parameter
    instead of hard-coded in the kernel implementation file.
    '''
    def simulate(observed_performance, stencil_name, jam,nk):
        import subprocess, re
        pattern = re.compile('^gen_(\w+): (\d+)')
        stencil_name = 'bagm_' + stencil_name
        jam = Jam(*jam)
        klength = nk       # Could estimate this in terms of the width of the kernel or whatever

        k_repeats = 3

        output = subprocess.check_output(['codegen/build.py', '--dry-run', '--kernel', stencil_name, '-v', '1', '-i', '1', '--unroll_i', str(jam.i), '--unroll_j', str(jam.j), '--k_repeats', str(k_repeats)])
        def normalize(key, val): return (key, int(val))
        kargs = dict(normalize(*pattern.search(line).groups()) for line in list(output.decode().splitlines()) if line.startswith('gen_'))
        if (args.verbose): print(kargs)
        kernel = Kernel(stencil_name=stencil_name, jam=jam, k_repeats=k_repeats, **kargs)
        skernel = "%s_%dx%d" %(stencil_name, jam.i, jam.j)
        print('%16s\t%f\t%.1f\t\t%f\t%.2f ' % 
              (skernel, kernel.mstencils(klength), kernel.cycles_per_stencil(klength), observed_performance[skernel], 100* observed_performance[skernel]/kernel.mstencils(klength)))
#        print('%12s_%dx%d\t\t%f           \t               ' % (stencil_name, jam.i, jam.j, kernel.mstencils(klength)))
    
    import csv
    observed_performance_file = 'results/performance-table.csv'
    ir = csv.reader(open(observed_performance_file, newline='\n'), delimiter=',')
    # skip header
    header = ir.__next__()
    results = list(ir)
    observed_performance = {}
    for l in results:
        if '14' in l[6]:
            okernel = 'bagm_'
            if 'load' in l[1]:
                if '7' in l[9]:
                    okernel += 'lc2x3_7pt'
                else:
                    okernel += 'lc_3pt'
            else:
                if '27' in l[9]:
                    okernel += 'mute_27pt'
                else:
                    okernel += 'mute_7pt'
            okernel += '_' + l[3][0:1] + 'x' + l[4][0:1]
            
            observed_performance.update({okernel:float(l[8])})
    print('\t\t\tMstencil/s\tcycles/stencil\tobserved\t%')
# old simulator values (for SC'11)
#    kernel_k ={'mute_27pt':302,    'mute_27pt':302,    'mute_27pt':266,    'mute_27pt':250,    'mute_27pt':198, 'mute2x3_7pt':246, 'lc2x3_7pt':246,        'lc_3pt':302,    'lc_3pt':302,    'lc_3pt':302,    'lc_3pt':294,    'lc_3pt':302}

# new values that are of the same size as the observed for the L1 experiments 
    kernel_k ={'mute_27pt':14, 'mute_27pt':14, 'mute_27pt':14, 'mute_27pt':14, 'mute_27pt':14, 'mute2x3_7pt':14, 'lc2x3_7pt':14, 'lc_3pt':14, 'lc_3pt':14, 'lc_3pt':14, 'lc_3pt':14, 'lc_3pt':14}
    
    

    for k in itertools.chain([('mute_27pt',jam,kernel_k['mute_27pt']) for jam in [(1,1), (1,2), (1,3), (2,2), (2,3)]],
                             [('mute_7pt' ,jam,kernel_k['mute2x3_7pt']) for jam in [(2,3)]],
                             [('lc2x3_7pt' ,jam,kernel_k['lc2x3_7pt']) for jam in [(2,3)]],
                             [('lc_3pt',jam,kernel_k['lc_3pt']) for jam in [(1,1), (2,1), (2,2), (2,3), (2,4)]]):
        
        simulate(observed_performance, *k)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Performance model for 27-point stencil using 2x2 jam')
    parser.add_argument('-k', help='Length of input array in direction k', dest='k', type=int, default=64)
    parser.add_argument('--plot', help='Plot throughput', action='store_true')
    parser.add_argument('--table', help='Make a performance table', action='store_true')
    parser.add_argument('--verbose', help='More verbose output', action='store_true')
    args = parser.parse_args()
    if args.table:
        make_table(args)
    else:
        kernel = mute_2x2_27pt
        print('%f Mstencil/s' % kernel.mstencils(args.k))
        if args.plot:
                import pylab
                import numpy
                k = numpy.arange(4,args.k,4)
                pylab.plot(k,pylab.amap(kernel.mstencils,k))
                pylab.show()


