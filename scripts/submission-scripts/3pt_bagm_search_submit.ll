#!/usr/bin/env bash
#
# @ job_name            = bagm_search
# @ job_type            = bluegene
# @ output              = ./$(job_name)_$(jobid).out
# @ error               = ./$(job_name)_$(jobid).err
# @ environment         = COPY_ALL; 
# @ wall_clock_limit    = 1:00:00,1:00:00
# @ notification        = always
# @ bg_size             = 128
# @ cluster_list        = bgp
# @ class               = smallblocks
# @ account_no          = c03

# @ queue


# Required problem sizes for the performance table:
#27: L1: 4,14,14
#    DRAM: 254, 254, 64
#7:  L1: 4,14,14
#    DRAM: 64, 254, 254
#3:  L1: 4,14,14
#    DRAM: 64, 254, 254

#for comparizon with Datta:
#   to compare with his results:
#       use: -DN_CYCLES=1 -DFLUSH_ALL_CACHES=1
#   to compare with Datta's upper bound:
#       use in L3 problem of the longest possible strip in k, from Datta's thesis on the upper bound experiments:
#       "we first chose a small stencil problem that fit into the last-level (largest) cache of a single socket on each of the systems in our study. The grid dimensions of this stencil problem were chosen with a long unit-stride dimension and a relatively short least contiguous dimension so as to avoid having many discontinuities in the memory access pattern. We then performed at least 100 stencil sweeps over this grid in order to amortize the initial time needed to retrieve data from DRAM into cache." - Datta thesis page 95



#3:  L1: 4,14,14
#    DRAM: 64, 254, 254
cd codegen

results_dir=../results/3pt_performance/
#list_kernels=(bagm_mute_27pt bagm_lc_3pt bagm_mute_7pt bagm_lc2x3_7pt) 
kernel=bagm_lc_3pt

list_mem_actions=("-DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000 " "-DFLUSH_ALL_CACHES=1 -DN_CYCLES=1 " )
list_domains=("-DN_I=4 -DN_J=14 -DN_K=14" "-DN_I=254 -DN_J=254 -DN_K=64" "-DN_I=64 -DN_J=254 -DN_K=254")
list_fma_switch=("--no_fma 1" "")
fma_switch_name=("_nofma" "")
i_unroll=(1 2 2 2 2)
j_unroll=(1 1 2 3 4)

for fma_switch in 0 1
do
results_file=${results_dir}lc_vanilla${fma_switch_name[$fma_switch]}.txt
echo "==========================================================================" | tee ${results_file}
echo "== run date:" $(date) | tee -a ${results_file}
echo "==========================================================================" | tee -a ${results_file}

for i_mem in 0 1
do
for i_dom in 0 2
do
for unroll in 0 1 2 3 4
do

DRIVE_OPTS=' DRIVER_OPTS=" -DVERIFY='$fma_switch' -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  '${list_domains[$i_dom]}' '${list_mem_actions[$i_mem]}'"'
BUILD_OPTS=' BUILDER_OPTS="-v 0 -i 1 '${list_fma_switch[$fma_switch]}' --unroll_i '${i_unroll[$unroll]}' --unroll_j '${j_unroll[$unroll]}' "'
echo "==========================================================================" | tee -a ${results_file}
echo 'make MUTANT_KERNEL='$kernel $DRIVE_OPTS $BUILD_OPTS' -B' | tee -a ${results_file}
echo "--------------------------------------------------------------------------" | tee -a ${results_file}
make MUTANT_KERNEL=$kernel DRIVER_OPTS="-DVERIFY=$fma_switch -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  ${list_domains[$i_dom]} ${list_mem_actions[$i_mem]}" BUILDER_OPTS="-v 0 -i 1 ${list_fma_switch[$fma_switch]} --unroll_i ${i_unroll[$unroll]} --unroll_j ${j_unroll[$unroll]} " -B | tee -a ${results_file}

echo "--------------------------------------------------------------------------" | tee -a ${results_file}

echo '/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/'$kernel ' ' | tee -a ${results_file}
echo "--------------------------------------------------------------------------" | tee -a ${results_file}
sleep 5
/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/$kernel  | tee -a ${results_file}

done
done
done
done


