#!/usr/bin/env python

import sys
import os
import itertools
import numpy as np
from collections import namedtuple
sys.path.append(os.path.abspath(os.path.curdir))

import parse_performance_output

record = namedtuple('record', 'itest idepend hazard fill time cycles')

def parse():
    import os
    in_csv_name = 'bootstrap_raw.csv'
    work_path = os.path.join(os.path.curdir, 'results/bootstrap_bgp')

    infile =  os.path.join(work_path,in_csv_name)
    
    with open(infile) as file:
        res = parse_performance_output.from_csv(file)
            
    results = {}
    for k in list(res[0]):
        results.update({k[0]:[record(itest=k[1], idepend=k[2], hazard=k[3], fill=k[4], time=k[5], cycles=k[6]),0]})

    return results

def construct_table(results): 
    out_csv_name = 'bootstrap_parsed.csv' 
    work_path = os.path.join(os.path.curdir, 'results/bootstrap_bgp')
    out_csv_file = os.path.join(work_path,out_csv_name)
    print("Computing the diff with the no hazard instructions")
    thresh = 0.2
    table = []
    for k in results.iterkeys():
        if 'noh' not in results[k][0].hazard:
            noh_test = results[k][0].itest+'_'+results[k][0].idepend+'_noh_'+results[k][0].fill
            diff = float(results[k][0].cycles) - float(results[noh_test.replace(' ', '')][0].cycles)
            results[k][1] = diff
            if abs(diff) > thresh: 
                table.append([k]+list(results[k][0])+["%2.2f" % results[k][1]])

    head = ('kernel_name', 'I_test', 'I_depend', 'hazard', 'fill_size', 'time/cycle', 'cycles/iteration', '-noh')    
    with open(out_csv_file, 'wb') as file:
        parse_performance_output.to_csv(table, file,head=head)

results = parse()
construct_table(results)

