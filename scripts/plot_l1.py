import numpy as np
import matplotlib.pyplot as plt
from parse_output import parse_output

with open('results/4_4_X/test_l1.log') as file:
    results = parse_output(file)
    
plt.plot(results[:,2],results[:,3])
plt.xlabel('Elements in K')
plt.ylabel('MStencils/s')
plt.show()
