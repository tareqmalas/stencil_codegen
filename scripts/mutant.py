"""Prototype performance model of sparse matrix operations on multicore architectures"""
from math import *
from pprint import PrettyPrinter

K  = 1e3
M  = 1e6
G  = 1e9
Ki = pow(1024,1)
Mi = pow(1024,2)
Gi = pow(1024,3)

class PPC450d:
    """PowerPC 450d CPU as seen on Blue Gene/P architectures"""
    def __init__(self):
        self.name = "PPC450d"
        self.clock_rate = 850*M
        self.mac_issues = 2
        self.L1_size = 32*Ki
        self.L1_bandwidth = 8
        self.L3_bandwidth = 4.6

#        self.L1_line_width = 8
#        self.L3_size = 8*Mi
#        self.L3_line_width=128
#        self.main_store_size = 4*Gi
#        self.main_store_max_bw = 3.7

class DenseMatrixMatrixProduct:
    """Dense Matrix-Matrix Product model for C = C + A*B"""
    def __init__(self, A, B):
        m = A.m
        n = A.n
        p = B.n
        self.name = "Dense Matrix-Matrix Product with %dx%d and %dx%d matrix factors" % (m, n, n, p)
        self.reads = m*p + m*n + n*p
        self.writes = m*p
        self.multiplies = m*p*n
        self.adds = m*p*n

class DenseMatrixVectorProduct:
    """Dense Matrix-Vector Product model for y = y + A*x"""
    def __init__(self, A):
        m = A.m
        n = A.n
        self.name = "Dense Matrix-Vector Product with %dx%d matrix" % (m, n)
        self.reads = m + m*n + n
        self.writes = m
        self.multiplies = m*n
        self.adds = m*n

class DenseMatrix:
    """Dense Matrix model"""
    def __init__(self, m, n):
        self.m = m
        self.n = n

def TheoreticalPeak(arch, algo):
    """Determines algorithm's theoretical peak by floating-point unit or memory bandwidth and returns it.
    We are currently assuming a dense matrix that fits completely in cache and no branching 
    """

    # an ideal algorithm will perform 2 floating point operations (multiply and carry) per mac issue
    peak_gflops = 2*arch.clock_rate*arch.mac_issues/G
    
    # the floating-point-unit lower bound for compute time is calculated by dividing the number of required operations by the rate at which they can be issued (with corrections for imbalance between multiplies and additions)
    macs = min(algo.multiplies,algo.adds)
    single_ops = macs - max(algo.multiplies, algo.adds)
    fpu_bound_gflops = peak_gflops*(1*macs + 0.5*single_ops)/(macs+single_ops)
    fpu_bound_compute_time_ns = (algo.multiplies+algo.adds)/fpu_bound_gflops

    # the memory subsystem lower bound for compute time is calculated by dividing the number of required loads and stores by the rate at which they can be issued
    L1_read_bytes = min(algo.reads*8, arch.L1_size)
    L3_read_bytes = algo.reads*8-L1_read_bytes
    L1_write_bytes = min(algo.writes*8, arch.L1_size)
    L3_write_bytes = algo.writes*8-L1_write_bytes

    read_time_ns = (L1_read_bytes/(arch.clock_rate*arch.L1_bandwidth) + L3_read_bytes/(arch.clock_rate*arch.L3_bandwidth))*G
    write_time_ns = (L1_write_bytes/(arch.clock_rate*arch.L1_bandwidth) + L3_write_bytes/(arch.clock_rate*arch.L3_bandwidth))*G

    mem_bound_compute_time_ns = read_time_ns + write_time_ns
#    mem_bound_compute_time_ns = ((algo.reads+algo.writes)/(arch.clock_rate*arch.load_store_issues))*G

    bound_compute_time_ns = max(fpu_bound_compute_time_ns, mem_bound_compute_time_ns)
    bound_gflops = (algo.multiplies+algo.adds)/bound_compute_time_ns

    print '%s\ntheoretically attains %e GFLOP/s on %s' % (algo.name, bound_gflops, arch.name) 
    return bound_gflops
    
if __name__ == "__main__":
    core_arch = PPC450d()
    A = DenseMatrix(32, 32)
    B = DenseMatrix(32, 32)
    algorithm = DenseMatrixMatrixProduct(A, B)
    pp = PrettyPrinter()
    pp.pprint(core_arch.__dict__)
    pp.pprint(algorithm.__dict__)
    TheoreticalPeak(core_arch,algorithm)
    algorithm = DenseMatrixVectorProduct(A)
    bound_gflops = TheoreticalPeak(core_arch,algorithm)
