#!/usr/bin/env python

import sys
import os
sys.path.append(os.path.abspath(os.path.curdir))

import parse_output

def parse(stencil, kernel, mode):
    import os
    print('parsing :',stencil, kernel, mode)
    in_file_name = kernel+'_'+mode+'.txt'
    out_csv_name = kernel+'_'+mode+'.csv' 
    out_pickle_name = kernel+'_'+mode+'.pickle' 

    work_path = os.path.join(os.path.curdir, 'results', stencil)

    infile =  os.path.join(work_path,in_file_name)
    out_csv_file = os.path.join(work_path,out_csv_name)
    out_pickle_file = os.path.join(work_path,out_pickle_name)
    
    with open(infile) as file:
        results = parse_output.to_list(file)
    for r in results:
        r.insert(0,mode)
            
    with open(out_csv_file, 'wb') as file:
        parse_output.to_csv(results, file)

    with open(out_pickle_file, 'wb') as file:
        parse_output.to_pickle(results, file)

stencil = '27pt'
kernel = 'mm'
for mode in ('l2pfo','vanilla'):
    parse(stencil, kernel, mode)

stencil = '3pt'
kernel = 'lc'
for mode in ('l2pfo','vanilla'):
    parse(stencil, kernel, mode)

stencil = '7pt'
for kernel in ('mm','lc'):
    for mode in ('l2pfo','vanilla'):
        parse(stencil, kernel, mode)    

