#!/usr/bin/env python

import sys
import os
import itertools
import numpy as np
sys.path.append(os.path.abspath(os.path.curdir))

import parse_performance_output

def parse(stencil, kernel, mode):
    import os
    print('parsing :',stencil, kernel, mode)
    in_file_name = kernel+'_'+mode+'.txt'
    out_csv_name = kernel+'_'+mode+'.csv' 
    out_pickle_name = kernel+'_'+mode+'.pickle' 

    work_path = os.path.join(os.path.curdir, 'results', stencil)

    infile =  os.path.join(work_path,in_file_name)
    out_csv_file = os.path.join(work_path,out_csv_name)
    out_pickle_file = os.path.join(work_path,out_pickle_name)
    
    with open(infile) as file:
        results = parse_performance_output.to_list(file)
    for r in results:
        r.insert(0,mode)
            
    with open(out_csv_file, 'wb') as file:
        parse_performance_output.to_csv(results, file)

    with open(out_pickle_file, 'wb') as file:
        parse_performance_output.to_pickle(results, file)

def construct_perf_table(files):
    
    results = []
    for stencil,kernel,mode in files:
        print('searching :',stencil, kernel, mode)
        in_csv_name = kernel+'_'+mode+'.csv'
        work_path = os.path.join(os.path.curdir, 'results', stencil)
        in_csv_file = os.path.join(work_path,in_csv_name)
        with open(in_csv_file, 'r') as file:
            res, header = parse_performance_output.from_csv(file)
            res = np.array(res)
            operator = int(stencil[0]) if int(stencil[0])!=2 else 27 
            res = np.append(res, np.ones((res.shape[0],1))*operator,1)
            results.extend(list(res))
    header += ['operator']
    results = np.array(results)        
    
    mstencils = results[:,-2].astype(float)
    new_ind = np.argsort(mstencils)[ ::-1]
    results = results[new_ind,:]

    head = {header[i]:i for i in range(len(header))}
    unique_key = set()
    table = np.array([])
    for k in results:
        key = (k[head['kernel']],k[head['jam_i']],k[head['jam_j']],
               k[head['ni']],k[head['nj']],k[head['nk']])
        if key not in unique_key:
            table = np.append(table,k)
            unique_key.add(key)
    table = np.reshape(table,(-1,10))
 
    out_csv_name = 'performance-table.csv'
    work_path = os.path.join(os.path.curdir, 'results')
    out_csv_file = os.path.join(work_path,out_csv_name)
    with open(out_csv_file, 'wb') as file:
        parse_performance_output.to_csv(table, file)
            
def files_list(modes):
    return itertools.chain([('27pt_performance', 'mm', mode) for mode in modes],
                         [('3pt_performance', 'lc', mode) for mode in modes],
                         [('7pt_performance','lc',mode) for mode in modes],
                         [('7pt_performance','mm',mode) for mode in modes])

for k in files_list(['vanilla', 'vanilla_nofma','l2pfo', 'l2pfo_nofma']): parse(*k)
    
construct_perf_table(files_list(('vanilla','l2pfo')))

