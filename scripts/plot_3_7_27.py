#!/usr/bin/env python

import sys
import os
sys.path.append(os.path.abspath(os.path.curdir))

import numpy as np
import pylab

import parse_output

out_path = os.path.join(os.path.curdir,'results')

class result_line():
    def __init__(self,x,y,format_string,label,legend):
        self.x = x
        self.y = y
        self.format_string = format_string
        self.label = label
        self.legend = legend
    def plot(self):
        return pylab.plot(self.x,self.y,self.format_string, label=self.label),self.label,self.legend
        
def plot(lines):
    lcmp = lambda x,y: cmp(y.label,x.label)
    lines.sort(lcmp)

    handles = []
    labels = []
    for l in lines:
        handle, label, legend = l.plot()
        if legend:
            handles.append(handle)
            labels.append(label)
        pylab.hold(True)
    fig = pylab.gcf()
    #   fig.legend(handles,labels,'center right')
    pylab.xlabel('Input length')
    pylab.ylabel('Mstencil/s')

    return handles, labels 
 

def plot3():
    stencil = '3pt'
    work_path = os.path.join(os.path.curdir, 'results', stencil)

    # one line per: BGP mode, total number of jams
    # x data : cube length (max(ni,nj,nk))
    # y data : mstencil/s
    kernel = 'lc'
    lines = []


    for bgp_mode,mode_name,line_char in [('l2pfo','l2pfoff',':'), ('vanilla','l2pfon','-')]:
        csv_file = os.path.join(work_path, kernel+'_'+bgp_mode+'.csv')
        with open(csv_file) as file:
            results = parse_output.from_csv(file)
        jams = parse_output.to_jam_arrays(results)
        color_list = ['b','g','r','k','m','c']
        for jam, result in jams.iteritems():
            length = [max(map(float,m)) for m in result['n']]
            mstencils = result['mstencils']
            format_string = color_list.pop()+line_char
            name = '%dx%d' % (jam[0],jam[1])
            legend = True
            if line_char == ':':
                legend=False
            lines.append(result_line(length, mstencils, format_string, name, legend))

    # bandwidth bound for 2x3 jam
    ni_length    = [0,    400]
    ni_mstencils = [231, 231]
    format_string = 'k--'
    name = 'Streaming \nBW peak'
    legend =True
    lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))


    handles, labels = plot(lines)
    pylab.legend(handles,labels, 'upper right')
#    pylab.show()

    pylab.savefig(os.path.join(out_path,'plot_'+stencil+'_'+kernel))
    
def plot7():
    stencil = '7pt'
    work_path = os.path.join(os.path.curdir, 'results', stencil)

    # one line per: BGP mode, total number of jams
    # x data : cube length (max(ni,nj,nk))
    # y data : mstencil/s
    kernel = 'lc'
    lines = []

#    for bgp_mode,mode_name,line_char in [('l2pfo','L2 optimistic prefetch disabled',':'), ('vanilla','L2 optimistic prefetch on','-')]:
    for bgp_mode,mode_name,line_char in [('l2pfo','',':'), ('vanilla','o','-')]:

        csv_file = os.path.join(work_path, kernel+'_'+bgp_mode+'.csv')
        with open(csv_file) as file:
            results = parse_output.from_csv(file)
        jams = parse_output.to_jam_arrays(results)
        color_list = ['b','g']
        for jam, result in jams.iteritems():
            length = [max(map(float,m)) for m in result['n']]
            mstencils = result['mstencils']
            format_string = color_list.pop()+line_char
            name = '%s_%dx%d%s' % (kernel,jam[0],jam[1],mode_name)
            legend = True
            if line_char == ':':
                legend=True
            lines.append(result_line(length, mstencils, format_string, name, legend))

    kernel = 'mm'

#    for bgp_mode,mode_name,line_char in [('l2pfo','L2 optimistic prefetch disabled',':'), ('vanilla','L2 optimistic prefetch on','-')]:
    for bgp_mode,mode_name,line_char in [('l2pfo','',':'), ('vanilla','o','-')]:
        csv_file = os.path.join(work_path, kernel+'_'+bgp_mode+'.csv')
        with open(csv_file) as file:
            results = parse_output.from_csv(file)
        jams = parse_output.to_jam_arrays(results)
        color_list = ['r','k']
        for jam, result in jams.iteritems():
            length = [max(map(float,m)) for m in result['n']]
            mstencils = result['mstencils']
            format_string = color_list.pop()+line_char
            name = '%s_%dx%d%s' % (kernel,jam[0],jam[1],mode_name)
            legend = True
            if line_char == ':':
                legend=True
            lines.append(result_line(length, mstencils, format_string, name, legend))

    # bandwidth bound for 2x3 jam
    ni_length    = [0,    400]
    ni_mstencils = [116.84, 116.84]
    format_string = 'k--'
    name = 'Streaming \nBW peak'
    legend =True
    lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))
 
    ni_length    = [0,    400]
    ni_mstencils = [140.53, 140.53]
    format_string = 'k:'
    name = 'L3 BW peak'
    legend =True
    lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))
 
    handles, labels = plot(lines)
    pylab.legend(handles,labels, 'upper right')
#    pylab.show()

    pylab.savefig(os.path.join(out_path,'plot_'+stencil))


def plot27():
    stencil = '27pt'
    work_path = os.path.join(os.path.curdir, 'results', stencil)

    # one line per: BGP mode, total number of jams
    # x data : cube length (max(ni,nj,nk))
    # y data : mstencil/s
    kernel = 'mm'
    lines = []

#    for bgp_mode,mode_name,line_char in [('l2pfo','l2pfoff',':'), ('vanilla','l2pfon','-')]:
    for bgp_mode,mode_name,line_char in [('l2pfo','l2pfoff','-')]:

        csv_file = os.path.join(work_path, kernel+'_'+bgp_mode+'.csv')
        with open(csv_file) as file:
            results = parse_output.from_csv(file)
        jams = parse_output.to_jam_arrays(results)
        color_list = ['b','g','r','k','m','c']
        for jam, result in jams.iteritems():
            ni_length = []
            ni_mstencils = []
            nk_length = []
            nk_mstencils = []

            for m,mstencils in zip(result['n'],result['mstencils']):
                ni,nj,nk = map(float,m)
                length = max(ni,nj,nk)
                if ni > nk:
                    ni_length.append(length)
                    ni_mstencils.append(mstencils)
                else:
                    nk_length.append(length)
                    nk_mstencils.append(mstencils)

            base_format_string = color_list.pop()+line_char
            format_string = base_format_string 
            name = '%dx%d' % (jam[0],jam[1])
            legend = True
            if line_char == ':':
                legend=False
            lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))

            format_string = base_format_string
            name = '%dx%d_k' % (jam[0],jam[1])
            legend = True
#            lines.append(result_line(nk_length, nk_mstencils, format_string, name, legend))
    
    # bandwidth bound for 2x3 jam
#    ni_length    = [14,    75,    75,    362]
#    ni_mstencils = [175.5, 175.5, 49.43, 49.43]
#    ni_length    = [0,    400]
#    ni_mstencils = [49.43, 49.43]
#    format_string = 'k:'
#    name = 'Streaming \nBW peak'
#    legend =True
#    lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))
    # compute bound for 2x3 jam
    ni_length    = [0,    400]
    ni_mstencils = [62.96, 62.96]
    format_string = 'k--'
    name = 'Compute \npeak'
    legend =True
    lines.append(result_line(ni_length, ni_mstencils, format_string, name, legend))

    handles, labels = plot(lines)
    pylab.legend(handles,labels, 'lower right')
#    pylab.show()
    pylab.savefig(os.path.join(out_path,'plot_'+stencil+'_'+kernel))
    
plot3()
pylab.clf()
plot7()
pylab.clf()
plot27()
