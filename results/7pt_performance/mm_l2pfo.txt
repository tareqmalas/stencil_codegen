==========================================================================
== run date: Wed Sep 7 14:53:31 AST 2011
==========================================================================
==========================================================================
make MUTANT_KERNEL=bagm_mute_7pt DRIVER_OPTS=" -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1 -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000 " BUILDER_OPTS="-v 0 -i 1 " -B
--------------------------------------------------------------------------
mkdir -p build
./build.py -k bagm_mute_7pt -v 0 -i 1  
gen_header_defines: 0
gen_initialize_registers: 0
gen_inner_iter: 76
gen_prologue: 30
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -Ibuild -o build/driver.o -c drivers/driver.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000  -qnosmp
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/c_kernel.o -c drivers/c_kernel.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000 
/bgsys/drivers/ppcfloor/comm/bin/mpixlf90_r -O5 -qextname -o build/fortran_kernel.o -c drivers/fortran_kernel.F90
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/bagm_mute_7pt build/driver.o build/c_kernel.o build/fortran_kernel.o  
--------------------------------------------------------------------------
/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/bagm_mute_7pt  
--------------------------------------------------------------------------
******************************************************
Verifying results
bagm-mute-7-point 2x3 jam Kernel:
     NORM:                   L1                   L2
349531.200000         4.572867e+08
Reference: 
349531.200000         4.572867e+08
******************************************************
******************************************************
TEST#01 time: 2.318748e-06
TEST#02 time: 2.317253e-06
TEST#03 time: 2.317176e-06
TEST#04 time: 2.318569e-06
******************************************************
Rank 0 of 4 <0,0,0,0>  R00-M0-N00-J23: Participating process 0 of 4


Rank 1 of 4 <0,0,0,1>  R00-M0-N00-J23: Participating process 1 of 4
Rank 2 of 4 <0,0,0,2>  R00-M0-N00-J23: Participating process 2 of 4

Rank 3 of 4 <0,0,0,3>  R00-M0-N00-J23: Participating process 3 of 4
******************************************************
Actions: 
L3 flushed: 0
L1 flushed: 0
L1 touched: 0

******************************************************
[bagm-mute-7-point 2x3 jam Kernel]
Jams in i: 2
Jams in j: 3
Number of tests    4
Number of stencils 288
Ni: 4 Nj: 14 Nk: 14 
Total memory allocation ~ 12544 B
total flops:       3.744000e-06 G 
total time(s)    : 9.271747e-06
time/test(s)     : 2.317937e-06
******************************************************
******************************************************
******************************************************
GFlop/s     AVG: 1.615230e+00  
MStencil/s  AVG: 124.248428  
******************************************************
GFlop/s     MAX: 1.615760e+00  
MStencil/s  MAX: 124.289196  
******************************************************
GFlop/s     MIN: 1.614664e+00  
MStencil/s  MIN: 124.204946  
******************************************************
==========================================================================
make MUTANT_KERNEL=bagm_mute_7pt DRIVER_OPTS=" -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1 -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000 " BUILDER_OPTS="-v 0 -i 1 " -B
--------------------------------------------------------------------------
mkdir -p build
./build.py -k bagm_mute_7pt -v 0 -i 1  
gen_header_defines: 0
gen_initialize_registers: 0
gen_inner_iter: 76
gen_prologue: 30
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -Ibuild -o build/driver.o -c drivers/driver.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000  -qnosmp
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/c_kernel.o -c drivers/c_kernel.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=0 -DN_CYCLES=1000 
/bgsys/drivers/ppcfloor/comm/bin/mpixlf90_r -O5 -qextname -o build/fortran_kernel.o -c drivers/fortran_kernel.F90
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/bagm_mute_7pt build/driver.o build/c_kernel.o build/fortran_kernel.o  
--------------------------------------------------------------------------
/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/bagm_mute_7pt  
--------------------------------------------------------------------------
******************************************************
Verifying results
bagm-mute-7-point 2x3 jam Kernel:
     NORM:                   L1                   L2
25198330700491.199219         2.117178e+20
Reference: 
25198330700491.199219         2.117178e+20
******************************************************
******************************************************
TEST#01 time: 6.923595e-02
TEST#02 time: 6.930425e-02
TEST#03 time: 6.924108e-02
TEST#04 time: 6.924241e-02
******************************************************
Rank 0 of 4 <0,0,0,0>  R00-M0-N00-J23: Participating process 0 of 4


Rank 1 of 4 <0,0,0,1>  R00-M0-N00-J23: Participating process 1 of 4
Rank 2 of 4 <0,0,0,2>  R00-M0-N00-J23: Participating process 2 of 4

Rank 3 of 4 <0,0,0,3>  R00-M0-N00-J23: Participating process 3 of 4
******************************************************
Actions: 
L3 flushed: 0
L1 flushed: 0
L1 touched: 0

******************************************************
[bagm-mute-7-point 2x3 jam Kernel]
Jams in i: 2
Jams in j: 3
Number of tests    4
Number of stencils 3937248
Ni: 64 Nj: 254 Nk: 254 
Total memory allocation ~ 66064384 B
total flops:       5.118422e-02 G 
total time(s)    : 2.770237e-01
time/test(s)     : 6.925592e-02
******************************************************
******************************************************
******************************************************
GFlop/s     AVG: 7.390592e-01  
MStencil/s  AVG: 56.850705  
******************************************************
GFlop/s     MAX: 7.392723e-01  
MStencil/s  MAX: 56.867102  
******************************************************
GFlop/s     MIN: 7.385438e-01  
MStencil/s  MIN: 56.811064  
******************************************************
==========================================================================
make MUTANT_KERNEL=bagm_mute_7pt DRIVER_OPTS=" -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1 -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1 " BUILDER_OPTS="-v 0 -i 1 " -B
--------------------------------------------------------------------------
mkdir -p build
./build.py -k bagm_mute_7pt -v 0 -i 1  
gen_header_defines: 0
gen_initialize_registers: 0
gen_inner_iter: 76
gen_prologue: 30
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -Ibuild -o build/driver.o -c drivers/driver.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1  -qnosmp
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/c_kernel.o -c drivers/c_kernel.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=4 -DN_J=14 -DN_K=14 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1 
/bgsys/drivers/ppcfloor/comm/bin/mpixlf90_r -O5 -qextname -o build/fortran_kernel.o -c drivers/fortran_kernel.F90
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/bagm_mute_7pt build/driver.o build/c_kernel.o build/fortran_kernel.o  
--------------------------------------------------------------------------
/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/bagm_mute_7pt  
--------------------------------------------------------------------------
******************************************************
Verifying results
bagm-mute-7-point 2x3 jam Kernel:
     NORM:                   L1                   L2
349531.200000         4.572867e+08
Reference: 
349531.200000         4.572867e+08
******************************************************
******************************************************
TEST#01 time: 7.080000e-06
TEST#02 time: 6.425882e-06
TEST#03 time: 6.152941e-06
TEST#04 time: 5.995294e-06
******************************************************
Rank 0 of 4 <0,0,0,0>  R00-M0-N00-J23: Participating process 0 of 4



Rank 1 of 4 <0,0,0,1>  R00-M0-N00-J23: Participating process 1 of 4
Rank 2 of 4 <0,0,0,2>  R00-M0-N00-J23: Participating process 2 of 4
Rank 3 of 4 <0,0,0,3>  R00-M0-N00-J23: Participating process 3 of 4
******************************************************
Actions: 
L3 flushed: 1
L1 flushed: 0
L1 touched: 0

******************************************************
[bagm-mute-7-point 2x3 jam Kernel]
Jams in i: 2
Jams in j: 3
Number of tests    4
Number of stencils 288
Ni: 4 Nj: 14 Nk: 14 
Total memory allocation ~ 12544 B
total flops:       3.744000e-06 G 
total time(s)    : 2.565412e-05
time/test(s)     : 6.413529e-06
******************************************************
******************************************************
******************************************************
GFlop/s     AVG: 5.837659e-01  
MStencil/s  AVG: 44.905072  
******************************************************
GFlop/s     MAX: 6.244898e-01  
MStencil/s  MAX: 48.037677  
******************************************************
GFlop/s     MIN: 5.288136e-01  
MStencil/s  MIN: 40.677966  
******************************************************
==========================================================================
make MUTANT_KERNEL=bagm_mute_7pt DRIVER_OPTS=" -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1 -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1 " BUILDER_OPTS="-v 0 -i 1 " -B
--------------------------------------------------------------------------
mkdir -p build
./build.py -k bagm_mute_7pt -v 0 -i 1  
gen_header_defines: 0
gen_initialize_registers: 0
gen_inner_iter: 76
gen_prologue: 30
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -Ibuild -o build/driver.o -c drivers/driver.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1  -qnosmp
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/c_kernel.o -c drivers/c_kernel.c -DVERIFY=1 -DN_TESTS=4 -DPRINT_TIME_DETAILS=1  -DN_I=64 -DN_J=254 -DN_K=254 -DFLUSH_ALL_CACHES=1 -DN_CYCLES=1 
/bgsys/drivers/ppcfloor/comm/bin/mpixlf90_r -O5 -qextname -o build/fortran_kernel.o -c drivers/fortran_kernel.F90
/bgsys/drivers/ppcfloor/comm/bin/mpixlc_r -O3 -qstrict -o build/bagm_mute_7pt build/driver.o build/c_kernel.o build/fortran_kernel.o  
--------------------------------------------------------------------------
/bgsys/drivers/ppcfloor/bin/mpirun -exp_env LD_LIBRARY_PATH -exp_env PYTHONPATH -env BG_MAPPING=TXYZ -np 4 -mode VN ./build/bagm_mute_7pt  
--------------------------------------------------------------------------
******************************************************
Verifying results
bagm-mute-7-point 2x3 jam Kernel:
     NORM:                   L1                   L2
25198330700491.199219         2.117178e+20
Reference: 
25198330700491.199219         2.117178e+20
******************************************************
******************************************************
TEST#01 time: 6.871497e-02
TEST#02 time: 6.894526e-02
TEST#03 time: 6.877581e-02
TEST#04 time: 6.910241e-02
******************************************************
Rank 0 of 4 <0,0,0,0>  R00-M0-N00-J23: Participating process 0 of 4


Rank 1 of 4 <0,0,0,1>  R00-M0-N00-J23: Participating process 1 of 4

Rank 3 of 4 <0,0,0,3>  R00-M0-N00-J23: Participating process 3 of 4
Rank 2 of 4 <0,0,0,2>  R00-M0-N00-J23: Participating process 2 of 4
******************************************************
Actions: 
L3 flushed: 1
L1 flushed: 0
L1 touched: 0

******************************************************
[bagm-mute-7-point 2x3 jam Kernel]
Jams in i: 2
Jams in j: 3
Number of tests    4
Number of stencils 3937248
Ni: 64 Nj: 254 Nk: 254 
Total memory allocation ~ 66064384 B
total flops:       5.118422e-02 G 
total time(s)    : 2.755384e-01
time/test(s)     : 6.888461e-02
******************************************************
******************************************************
******************************************************
GFlop/s     AVG: 7.430429e-01  
MStencil/s  AVG: 57.157149  
******************************************************
GFlop/s     MAX: 7.448773e-01  
MStencil/s  MAX: 57.298255  
******************************************************
GFlop/s     MIN: 7.407010e-01  
MStencil/s  MIN: 56.977000  
******************************************************
