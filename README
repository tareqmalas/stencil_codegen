SYSTEM REQUIREMENTS:
    Code generation requires Python 3
    Compiling the code require IBM XL compiler


PROJECT STRUCTURE:
  The project has three major Directories:
    1) codegen: Contains the source code for running the framework. It has four major directories:
        *drivers: Contains the C and Fortran source code required to run, measure the time, and verify the correctness of the generated kernels. Also, a bootstrap driver to facilitate measuring instructions specifications
        *kernels: Contains code templates and Python source code to generate the kernels
        *build: Contains the generated kernel's source code (gen_kernel.ic) and the compiled binaries and object files
        *simasm: Created after the first make. It is a clone of the PowerPC simulator code repository
    2)scripts
    3)results

USAGE:
    The current directory must be "codegen" for proper operation
    
    CODE GENERATION:
    The code generation is driven by build.py Python code. It has the following usage:
        usage: build.py [-h] [-k KERNEL] [-i INTERLEAVE] [-v VERBOSE] [-n]
                    [--unroll_j UNROLL_J] [--unroll_i UNROLL_I] [-l]
        Build.py: Generates kernels
        optional arguments:
          -h, --help            show this help message and exit
          -k KERNEL, --kernel KERNEL
                                kernel to generate
          -i INTERLEAVE, --interleave INTERLEAVE
                                enable/disable instructions interleaving (default 1 "enabled")
          -v VERBOSE, --verbose VERBOSE
                                Show the log of the simulator (default 1 "enabled")
          -n, --dry-run         Do not write the generated kernel
          --unroll_j UNROLL_J   specify the unrolling size in J
          --unroll_i UNROLL_I   specify the unrolling size in I
          -l, --list            list available kernels
          --no_fma              generate the code without the FMA operations
    Example:
        python3.2 build.py -k bagm_mute_27pt -v 0 -i 1 --unroll_i 2 --unroll_j 2
    
    MAKE COMMAND:
    The make command runs the code generation and compiles the code to produce the binaries.
    The make command has three major options:
        MUTANT_KERNEL: The desired kernel to be generated (use "./build.py -l" to list them)
        BUILDER_OPTS: These are parameters to "build.py" described above
        DRIVER_OPTS: These options configure the driver code and set the problem size: 
            -DN_I, -DN_J, -DN_K: Set the domain size in the i, j, and k directions  
            -DN_TESTS: Number of experiment's repetition during the execution
            -DN_CYCLES: Number of kernel runs per test
            -DPRINT_TIME_DETAILS: Print the time of each test (0/1)
            -DVERIFY: Verify the correctness of the generated kernel (0/1)
            -DUSE_FORTRAN_KERNEL: use Fortran kernel in the verification (0/1)
            -DUSE_C_KERNEL: use C kernel in the verification (0/1)

    Example:
        make MUTANT_KERNEL=bagm_mute_27pt DRIVER_OPTS="-DUSE_FORTRAN_KERNEL=0 -DVERIFY=1 -DN_TESTS=3 -DN_CYCLES=1000 -DPRINT_TIME_DETAILS=1 -DN_I=12 -DN_J=14 -DN_K=12" BUILDER_OPTS="-v 0 -i 1 --unroll_i 2 --unroll_j 2" -B


SCRIPTS:
    SHAHEEN SUBMISSION SCRIPT (includes code generation, make, and run for all the kernels):
        This requires loading Python3.2 in Shaheen "module load kslrun", then submit "scripts/submission-scripts/bagm_search_submit.ll" to the load leveler to process the kernels of all the stencil operators. To process the kernels of a single operator use "scripts/submission-scripts/*pt_bagm_search_submit.ll" for the specific required operator.
  
    PARSING THE RESULTS:
    The script "scripts/parse_3_7_27_performance.py" parses the results and generates tables of the results under "results". The command of this script:
    $./scripts/parse_3_7_27_performance.py

