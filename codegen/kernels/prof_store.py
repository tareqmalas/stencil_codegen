from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile, PPC
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="0", verbose="0", unroll_i=2, unroll_j=2, k_repeats=1,no_fma=False):  
        self.max_fill = 1
        self.no_fma = no_fma
        self.use_trace = 0 #verbose=="1"
        restricted_gpr = [0,1,2,14,30,  # do not use
                          11,12,13]        # stack backup/restore
        self.gpr_pointers = [i for i in range(32) if i not in restricted_gpr]
#        if interleave=="1":
#            self.generate_code = self.print_scheduling
#        else:
        self.generate_code = self.print_no_scheduling
        self.n_pointers = 6
            
        # create function to deal with blocks of registers 
        self.c = get_core(use_trace=self.use_trace, no_fma=self.no_fma)
        self.cv = CViewer(self.c)

        # Allocate the fp         str += 'register int k asm ("%d");\n' % self.loop_index.numregisters 
        self.fpr = self.c.acquire_fpregisters(range(32))
        self.fpr_pool = [0, 1, 2, 3, 4, 26, 27, 28, 29, 30]

        self.loop_index = IntRegister(self.gpr_pointers.pop(),'loop_index')
        self.sixteen1 = IntRegister(self.gpr_pointers.pop(),'sixteen1')
        self.sixteen2 = IntRegister(self.gpr_pointers.pop(),'sixteen2')
        self.gpr = [IntRegister(self.gpr_pointers.pop(),'gpr%d' % i) for i in range(2*self.n_pointers)]              

        self.kernels_list = [self.kernel(self,i) for i in list(range(4,7))]
                                 
    def generate_code_append(self, istream, com, sched_stream, clock_count):
        sched, count = self.generate_code(istream,com)
        sched_stream[0] += sched
        clock_count[0] += count

    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        return inst_str, com.c.execute(istream)

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        count = com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str, count
        
    class kernel:
        def __init__(k_self, self, size):
            k_self.n_pointers = self.n_pointers
            k_self.size = size
            if size > self.n_pointers:
                raise Exception('fill size must be <=%d' % self.n_pointers)
            
        def c_func_name(k_self):
            return "performance_%d_stores" % k_self.size

        def c_struct_str(k_self):
            return '{"%s", %s, %d, "%s", "%s", "%s"}' \
                % (k_self.c_func_name(),k_self.c_func_name(),k_self.size,'','','')
            
    def gen_kernels_struct(self):
        struct_list = ',\n'.join([self.kernels_list[i].c_struct_str() for i in range(len(self.kernels_list))])                
        return struct_list, 0
    
    def generate_one_kernel(self, com, k_index):
        c_kernel = ['']
        istream = []
        clock_count = [0]
        
        c_kernel[0] += 'inline void %s() {\n' % com.kernels_list[k_index].c_func_name()

        for i in list(range(0, com.kernels_list[k_index].size*2, 2)):
#        for i in range(self.n_pointers):
            c_kernel[0] += '  %s = dummy%d;\n' % (self.gpr[i].c_var, int(i/2))      
            c_kernel[0] += '  %s = 0;\n'      % self.gpr[i+1].c_var
      
        c_kernel[0] += '  for(k=0; k<N_CYCLES; k++) {\n'        
        
        istream += [isa.stfpdux(self.fpr[int(i/2)], self.gpr[i], self.gpr[i+1]) for i in list(range(0, com.kernels_list[k_index].size*2, 2))]    

        self.generate_code_append(istream,com,c_kernel,clock_count)       
        c_kernel[0] += '  }\n}\n\n'        
        return c_kernel[0]
    
    def gen_kernels_func(self):
        all_funcs = ''        
 
        for i in range(len(self.kernels_list)):
            all_funcs += self.generate_one_kernel(self,i)
        
        return all_funcs, 0

    def gen_header_defines(self):
        str = ''
        str += '#define N_KERNELS     (%d)\n' % len(self.kernels_list)
        str += '\n'
        for i in range(self.n_pointers):
            str += 'volatile double dummy%d[2] = {23.234+%d.0, 54.344+%d.0};\n' % (i,i,i)
        for i in list(range(0,len(self.gpr)-1,2)):
            str += 'register volatile double * %s asm ("%d");\n' % (self.gpr[i].c_var,self.gpr[i].num)
            str += 'register int %s asm ("%d");\n' % (self.gpr[i+1].c_var,self.gpr[i+1].num)

        str += 'register int k asm ("%d");\n' % self.loop_index.num
        str += '\n#define MAX_FILL (%d)\n' % self.max_fill
        return str, 0

