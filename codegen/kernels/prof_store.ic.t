//-*- c -*-

struct Kernel {{
  const char *name;
  void (*func)(void);
  int fill;
  char *hazard;
  char *i_test;
  char *i_depend;
}};

{gen_header_defines!s}


{gen_kernels_func!s}

struct Kernel KernelList[] = {{
{gen_kernels_struct!s},
}};
