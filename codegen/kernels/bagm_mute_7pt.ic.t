//-*- c -*-
/*
FP Registers allocation:
    0-15:   Input data frame (j is fast moving dimension)
    16-19:  First set of jam results
    20-23:  Second set of jam results
    24-31:  weights coefficients
    
    Weights mapping in the stencil operator
    frame  k=0     k=1     k=2
            j       j       j
                    2          
        i   3     1 0 1     3  
                    2          
    
    Weight index   : 0  1  2  3
    Weight register: 28 29 30 31
*/
{gen_header_defines!s}

#define kernel_reference    kernel_reference_7pt
#define fortran_kernel_     fortran_kernel_7pt_

inline void gen_kernel(const double *restrict a, double *restrict r, const double *restrict w, int istride, int jstride) {{
//inline void gen_kernel(double *a, double *r, double *w, int istride, int jstride) {{
    // Allocate general purpose registers 
    // (do not touch 2, 14, or 30)
    register const double *restrict weights asm ("26");  
//    register int zero asm ("25");
    register int eight asm ("28"); 
    register int sixteen asm ("27"); 
    register int k asm ("31");

//    zero = 0; 
    eight = 8; // sizeof(double)
    sixteen = 16;


//    asm(".__initialize_registers:");
{gen_initialize_registers!s}

//    asm(".__prologue:");
{gen_prologue!s}
//    asm(".__inner_iter:");
    for (k=1; k < jstride-2; k+=2) {{
{gen_inner_iter!s}
    }}
}}

