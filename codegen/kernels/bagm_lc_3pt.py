"""
3-point stencil steps:
=======================================
            PROLOGUE
a1   ← A[i,j,k+0]      ;load to get ax0    
a2   ← A[i,j,k+1:k+2]  ;load to get a12  int  
a1.p  = a2.p           ;copy to get a10    

=======================================
            LOOP
r12   = a1 '* W0
a1  ← A[i,j,k+3:k+4]   ;load to get a34  int  
r12  += a2  * W1
a2.p  = a1.p           ;copy to get a32    
r12  += a2 '* W2
r12 → R[i,j,k+1:k+2]   
---------------------------------------
r34   = a2 '* W0                         
a2    ← A[i,j,k+5:k+6] ;load to get a56  int
r34  += a1  * W1
a1.p  = a2.p           ;copy to get a54  
r34  += a1 '* W2
r34 → R[i,j,k+3:k+4]   

=======================================
            EPILOGUE 
NONE

            
#r56   = a1 '* W0
#a1    ← A[i,j,k+7:k+8]  ;load to get a78
#r56  += a2  * W1
#a2.p  = a1.p            ;copy to get a76
#r56  += a2 '* W2
#r56 → R[i,j,k+5:k+6]
"""

from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="1", verbose="1",unroll_i=2,unroll_j=3, k_repeats=1,no_fma=False):
        self.no_fma = no_fma
        self.JAM_I = unroll_i
        self.JAM_J = unroll_j

        self.k_repeats = k_repeats
        
        self.JAM_SIZE = self.JAM_I * self.JAM_J

        self.K0 = 0
        self.K1 = 1
        self.K2 = 2
        self.use_trace = verbose=="1"
        
        self.frame_3pt = list(range(self.JAM_SIZE))
            
        restricted_gpr = [0,1,2,14,30, # do not use
                          31,    # loop index / stack backup pointer
                          27, # constants for the load update 16
                          26]    # weights pointer (this one can be abandoned later by changing its use after being consumed)
        self.gpr_pointers = [i for i in range(32) if i not in restricted_gpr]
        self.gpr_results_offset = self.JAM_SIZE
        
        k_name = "bagm-load-copy-3-point %dx%d jam Kernel" % (self.JAM_I,self.JAM_J)
        if interleave=="1":
            self.generate_code = self.print_scheduling
            self.kernel_name = k_name
        else:
            self.generate_code = self.print_no_scheduling
            self.kernel_name = k_name + " - no interleaving"
        if no_fma:
            self.kernel_name = k_name + " - FMA OPS DISABLED"
    def generate_code_append(self, istream, com, sched_stream, clock_count):
        sched, count = self.generate_code(istream,com)
        sched_stream[0] += sched
        clock_count[0] += count
        
    class Common:        
        def __init__(com_self, self):
            # create function to deal with blocks of registers 
            com_self.c = get_core(use_trace=self.use_trace, no_fma=self.no_fma)
            com_self.cv = CViewer(com_self.c)
            
            # Allocate fp registers 
            FPR_pool = com_self.c.acquire_fpregisters(range(30,-1,-1))
            # Allocate input data fp registers  
            com_self.streams_set1 = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.streams_set1[i] = FPR_pool.pop()
            com_self.streams_set2 = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.streams_set2[i] = FPR_pool.pop()
            # Allocate results fp registers
            com_self.results = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.results[i] = FPR_pool.pop()            
            
            # Allocate 4 fp registers for the weights
            com_self.w_unique = com_self.c.acquire_fpregisters(range(30,32))                        
            com_self.w = [0]*2
            com_self.w[0]= com_self.w_unique[0]
            com_self.w[1]= com_self.w_unique[1]
        
            # Allocate int registers
            # registers 14 and 30 are reserved, use at own risk
            # register 2 is causing error when used on Shaheen
            
            # Reserve integer registers for streams pointers 
            # Input data streams pointers
            streams_str = ['a%d%d' % (i//self.JAM_J,i%self.JAM_J) for i in range(self.JAM_SIZE)]
            com_self.streams_p = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.streams_p[i] = IntRegister(self.gpr_pointers[i],streams_str[i])
            # Results registers pointers
            results_str = ['r%d%d' % (i//self.JAM_J,i%self.JAM_J) for i in range(self.JAM_SIZE)]
            com_self.results_p = [IntRegister(self.gpr_pointers[i+self.JAM_SIZE],results_str[i]) for i in range(self.JAM_SIZE)]
    
            # constants
            com_self.sixteen = IntRegister(27,'sixteen')
            # Weight pointer
            com_self.w_p = IntRegister(26,'weights')

    def fma_block(self,w, streams, result, k_index):
        istream = []

        # Do the FMA's
        for i in range(self.JAM_SIZE): 
            if k_index == 0:   istream += [isa.fxmul(result[i], streams[i], w[1])]               
            elif k_index == 1: istream += [isa.fxcpmadd(result[i], w[0], streams[i], result[i])]
            elif k_index == 2: istream += [isa.fxcxma(result[i], w[1], streams[i], result[i])]
#            else:
#                raise Exception('k_index must be 0,1, or 2')
            
        return istream   
    
    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        return inst_str, com.c.execute(istream)    

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        count = com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str, count
    
    def gen_header_defines(self):
        stencil_size = 3
        str = ''
        str+='#define GEN_KERNEL_A_OFFSET     (1)\n'
        str+='#define GEN_KERNEL_R_OFFSET     (1)\n'
        str+='#define GEN_KERNEL_NAME ("%s")\n' % self.kernel_name
        str+='#define GEN_KERNEL_OPERATOR_SIZE  (%.1f)\n' % stencil_size
        str+='#define GEN_KERNEL_LS_CYCLES (%.1f)\n' % ((self.JAM_SIZE+self.JAM_SIZE)*2*2)             # *2 cycles, *2 iterations
        str+='#define GEN_KERNEL_FPU_CYCLES (%.1f)\n' % ((stencil_size*self.JAM_SIZE+self.JAM_SIZE)*2) # *2 iterations
        str+='#define GEN_KERNEL_NI_JAMS (%d)\n' % self.JAM_I
        str+='#define GEN_KERNEL_NJ_JAMS (%d)\n' % self.JAM_J
        str+='#define N_FLOPS (N_STENCILS*%.1f)\n' % (stencil_size*2-1)
        
        return str, 0
    
    def gen_initialize_registers(self):
        init_reg = ''
        
        # Reserve integer registers for streams pointers
        # define the integer registers
        
        # define the pointers of the input data strides
        for i in range(self.JAM_SIZE):
            init_reg += '    register double *a%d%d asm ("%d");\n' % \
            (i//self.JAM_J, i%self.JAM_J, self.gpr_pointers[i])
        init_reg += '\n'

        for i in range(self.JAM_SIZE):     # define the pointers of the results strides
            init_reg += '    register double *r%d%d asm ("%d");\n' % \
            (i//self.JAM_J, i%self.JAM_J, self.gpr_pointers[i+self.JAM_SIZE])
        init_reg += '\n'   
         
        # Assign the input data strides' pointers to the integer registers
        shift = list(range(max(self.JAM_I, self.JAM_J)))

        for i in range(self.JAM_SIZE):
            init_reg += \
            '    a%d%d = (double *) (a + %d*istride + %d*jstride - 1);\n' % \
            (i//self.JAM_J, i%self.JAM_J, shift[i//self.JAM_J], shift[i%self.JAM_J])    
        init_reg += '\n'   
        
        # Assign the results strides pointers to the integer registers
        shift = list(range(max(self.JAM_I, self.JAM_J)))
        for i in range(self.JAM_SIZE):
            init_reg += \
            '    r%d%d = (double *) (r + %d*istride + %d*jstride - 1);\n' % \
            (i//self.JAM_J, i%self.JAM_J, shift[i//self.JAM_J], shift[i%self.JAM_J])    
            
        # Assign the weights pointer to its integer register
        init_reg += '\n    weights = w - 2;\n'   
            
        return init_reg, 0

    def gen_prologue(self):
        com = self.Common(self)
        inst_str = ['']
        istream = []
        clock_count = [0]
                
        ## PROLOGUE
        
        # load 6 interior 1/2 input data-primary values for frame 1/3
        istream += [isa.lfsdux(com.streams_set1[i],com.streams_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
        
        self.generate_code_append(istream,com,inst_str,clock_count)       
        istream = []

        # do pointer shift
        for i in range(self.JAM_SIZE):
            inst_str[0] += '    a%d%d --;\n' % (i//self.JAM_J, i%self.JAM_J)    
        inst_str[0] += '\n'
        # change the constant from 8 to 16
        inst_str[0] += '    sixteen = 16;\n\n'
        
        # load weights registers
        for i in range(2): istream += [isa.lfpdux(com.w_unique[i],com.w_p,com.sixteen)]
        
        # load 6 interior 2/2 input data-pair values to prepare frame 2/3
        istream += [isa.lfpdux(com.streams_set2[i],com.streams_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
        
        # copy the interior primary from frame 2 to frame 1
        istream += [isa.fmr(com.streams_set1[i],com.streams_set2[i]) for i in range(self.JAM_SIZE)]
 
#        # we can move some FMA's here to make use of some stalled cycles:
#        # do the FMA's for              frame 1/3
#        istream += self.fma_block(com.w, com.streams_set1, com.results, self.K0)
#        # do the FMA's for              frame 2/3
#        for i in self.block_ind: istream += self.fma_block(com.w, com.streams_set2, com.results, i, self.K1)

        self.generate_code_append(istream,com,inst_str,clock_count)       
        istream = []
        
        return inst_str[0],clock_count[0]
    
    def gen_inner_iter(self):        
        com = self.Common(self)
        istream = []

        for k in range(self.k_repeats):        
            ## ODD ITERATION
            # do the FMA's for              frame 1/3
            istream += self.fma_block(com.w, com.streams_set1, com.results, self.K0)
            
            # load interior 1/2 input data-pair values to prepare frame 3/3
            istream += [isa.lfpdux(com.streams_set1[i],com.streams_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
            
            # do the FMA's for              frame 2/3
            istream += self.fma_block(com.w, com.streams_set2, com.results, self.K1)

            # copy the interior primary from frame 1 to frame 2 for frame 3/3
            istream += [isa.fmr(com.streams_set2[i],com.streams_set1[i]) for i in range(self.JAM_SIZE)]

            # do the FMA's for              frame 3/3
            istream += self.fma_block(com.w, com.streams_set2, com.results, self.K2)
            
            # write back                               result set 1/2
            istream += [isa.stfpdux(com.results[i],com.results_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
            
            
            ## EVEN ITERATION
            # do the FMA's for              frame 1/3
            istream += self.fma_block(com.w, com.streams_set2, com.results, self.K0)
            
            # load interior 1/2 input data-pair values to prepare frame 3/3
            istream += [isa.lfpdux(com.streams_set2[i],com.streams_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
            
            # do the FMA's for              frame 2/3
            istream += self.fma_block(com.w, com.streams_set1, com.results, self.K1)
	            
            # copy the interior primary from frame 1 to frame 2 for frame 3/3
            istream += [isa.fmr(com.streams_set1[i],com.streams_set2[i]) for i in range(self.JAM_SIZE)]

            # do the FMA's for              frame 3/3
            istream += self.fma_block(com.w, com.streams_set1, com.results, self.K2)
            
            # write back                               result set 1/2
            istream += [isa.stfpdux(com.results[i],com.results_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
        
        
        return self.generate_code(istream,com)


