//-*- c -*-
/*
FP Registers allocation:
    0-15:   Input data frame (j is fast moving dimension)
    16-19:  First set of jam results
    20-23:  Second set of jam results
    24-31:  weights coefficients
    
    Weights mapping in the stencil operator
    frame  k=0     k=1     k=2
            j       j       j
          7 4 7   6 2 6   7 4 7
        i 5 3 5   1 0 1   5 3 5
          7 4 7   6 2 6   7 4 7
    
    Weight index   : 0  1   2   3   4   5   6   7
    Weight register: 24 25  26  27  28  29  30  31
*/
{gen_header_defines!s}

#define kernel_reference    kernel_reference_27pt
#define fortran_kernel_     fortran_kernel_27pt_

inline void gen_kernel(const double *restrict a, double *restrict r, const double *restrict w, int istride, int jstride) {{
//inline void gen_kernel(double *a, double *r, double *w, int istride, int jstride) {{
    // Allocate general purpose registers 
    // (do not touch 2, 14, or 30)
    register const double *restrict weights asm ("26");  
    register int k asm ("31");
//    register int zero asm ("25");
//    register int eight asm ("28"); 
    register int sixteen asm ("27"); 

    register double * a_ptr asm ("3");
    register int next_frame asm ("4");
    register int next_j_jam asm ("5");
    register int next_i_jam asm ("6");
    
    next_frame = 1 - (istride) * (GEN_KERNEL_NI_JAMS+2-1) - jstride * (GEN_KERNEL_NJ_JAMS+2-1);
    next_j_jam = 8*jstride;
    next_i_jam = 8*(istride - jstride * (GEN_KERNEL_NJ_JAMS+2-1));
    
    a_ptr = (double *)a -istride - 1*jstride - next_frame; 
    next_frame=8*(next_frame);// first update in the prologue uses next_frame for index update

//    zero = 0; 
//    eight = 8; // sizeof(double)
    sixteen = 16;


//    asm(".__initialize_registers:");
{gen_initialize_registers!s}

//    asm(".__prologue:");
{gen_prologue!s}

a_ptr += 1;

//    asm(".__inner_iter:");
    for (k=1; k < jstride-2; k+=2) {{
{gen_inner_iter!s}
    }}
}}

