"""
7-point stencil steps:
=======================================
            PROLOGUE
a.s    ← A[i,j,k+0]      ;load to get ax0    int
a.p    ← A[i,j,k+1]      ;load to get a10    int

=======================================
            LOOP
r1_12   = a10 '* W0
a.s   ← A[i,j,k+2]       ;mute to get a12  int
a     ← A[i,j,k+1:k+2]   ;load to get a12  ext
r1_12  += a12  * W1

a.p     ← A[i,j,k+3]     ;mute to get a32  int  
r1_12  += a32 '* W2
r1_12 → R[i,j,k+1:k+2]   
"""

from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="1", verbose="1", unroll_i=2, unroll_j=3, k_repeats=1,no_fma=False):
        self.no_fma = no_fma
        self.JAM_I = unroll_i
        self.JAM_J = unroll_j

        self.k_repeats = k_repeats

        self.FRAME_I = self.JAM_I+2
        self.FRAME_J = self.JAM_J+2
        self.JAM_SIZE = self.JAM_I * self.JAM_J
        self.FRAME_SIZE = self.FRAME_I * self.FRAME_J

        self.K0 = 0
        self.K1 = 1
        self.K2 = 2
        self.use_trace = verbose=="1"
        
        self.frame_7pt = list(range(self.FRAME_SIZE))
        del self.frame_7pt[self.FRAME_SIZE-1]
        del self.frame_7pt[self.FRAME_SIZE-self.FRAME_J]
        del self.frame_7pt[self.FRAME_J-1]
        del self.frame_7pt[0]        

        self.block_ind = [1, self.FRAME_J, self.FRAME_J+1, self.FRAME_J+2, 2*self.FRAME_J+1]
#        self.block_ind = [    1,
#                          5 , 6, 7,
#                             11]
        self.interior_block_ind = self.FRAME_J+1

        # define the starting indices of the 2x3 sub-blocks
        #        *   1   *   *   *
        #        5   6   7   *   *
        #        *   11  *   *   *
        #        *   *   *   *   *
        self.interior_frame_7pt = [(self.interior_block_ind + i%self.JAM_J + self.FRAME_J*(i//self.JAM_J)) for i in range(self.JAM_SIZE)]
#        self.interior_frame_7pt =[    
#                                     6 ,7 ,8 , 
#                                     11,12,13
#                                                ]
        self.exterior_frame_7pt = list(set(self.frame_7pt).difference(set(self.interior_frame_7pt)))
#        self.exterior_frame_7pt =[   1 ,2 ,3 ,    
#                                  5 ,         9 , 
#                                  10,         14,
#                                     16,17,18  ]
        
            
        restricted_gpr = [0,1,2,14,30, # do not use
                          31,    # loop index / stack backup pointer
                          27,28, # constants for the load update (16,8)
                          26]    # weights pointer (this one can be abandoned later by changing its use after being consumed)
        self.gpr_pointers = [i for i in range(32) if i not in restricted_gpr]
        self.gpr_results_offset = self.frame_7pt.__len__()
        
        k_name = "bagm-mute-7-point %dx%d jam Kernel" % (self.JAM_I,self.JAM_J)
        if interleave=="1":
            self.generate_code = self.print_scheduling
            self.kernel_name = k_name
        else:
            self.generate_code = self.print_no_scheduling
            self.kernel_name = k_name + " - no interleaving"
        if no_fma:
            self.kernel_name = k_name + " - FMA OPS DISABLED"
    def generate_code_append(self, istream, com, sched_stream, clock_count):
        sched, count = self.generate_code(istream,com)
        sched_stream[0] += sched
        clock_count[0] += count
        
    class Common:        
        def __init__(com_self, self):
            # create function to deal with blocks of registers 
            com_self.c = get_core(use_trace=self.use_trace, no_fma=self.no_fma)
            com_self.cv = CViewer(com_self.c)
            
            # Allocate fp registers 
            FPR_pool = com_self.c.acquire_fpregisters(range(27,-1,-1))
            # preserve the indexing of 4x5 block by allocating dummy indices in the frame 
            # Allocate input data fp registers for the input data frame 
            com_self.streams = [0]*self.FRAME_SIZE
            for i in self.frame_7pt: com_self.streams[i] = FPR_pool.pop()
            # Allocate results fp registers
            com_self.results = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.results[i] = FPR_pool.pop()
            
            # Allocate 4 fp registers for the weights
            com_self.w_unique = com_self.c.acquire_fpregisters(range(28,32))            
            
            # Weights mapping to the 7-points stecil
            #    w ind: 0  1  2  3
            #    w reg: 28 29 30 31
            #
            #frame   f0      f1      f2
            #                 2
            #         3     1 0 1     3
            #                 2
            #
            #       0  1  2    9  10 11    18 19 20
            #       3  4  5    12 13 14    21 22 23
            #       6  7  8    15 16 17    24 25 26
            #
            # reserve 3x3x3 indexing for the 4 weights
            com_self.w = [0]*27
            
            com_self.w[4+9*1] = com_self.w_unique[0]
            com_self.w[3+9*1] = com_self.w[5+9*1] = com_self.w_unique[1]
            com_self.w[1+9*1] = com_self.w[7+9*1] = com_self.w_unique[2]
            com_self.w[4+9*0] = com_self.w[4+9*2] = com_self.w_unique[3]
        
            # Allocate int registers
            # registers 14 and 30 are reserved, use at own risk
            # register 2 is causing error when used on Shaheen
            
            # Reserve integer registers for streams pointers (corners are dummy to ease the indexing)
            # Input data streams pointers
            streams_str = ['a%d%d' % (i//self.FRAME_J,i%self.FRAME_J) for i in range(self.FRAME_SIZE)]
            com_self.streams_p = [0]*self.FRAME_SIZE
            for i in range(self.frame_7pt.__len__()): 
                com_self.streams_p[self.frame_7pt[i]] = IntRegister(self.gpr_pointers[i],streams_str[self.frame_7pt[i]])            
            # Results registers pointers
            results_str = ['r%d%d' % (i//self.JAM_J,i%self.JAM_J) for i in range(self.JAM_SIZE)]
            com_self.results_p = [IntRegister(self.gpr_pointers[i+self.gpr_results_offset],results_str[i]) for i in range(self.JAM_SIZE)]
    
            # constants
            com_self.eight = IntRegister(28,'eight')
            com_self.sixteen = IntRegister(27,'sixteen')
            # Weight pointer
            com_self.w_p = IntRegister(26,'weights')

    def fma_block(self,w ,streams, result, stream_index, k_index):
        istream = []
        # Determine the current weight coefficient
        weight = w[stream_index%self.FRAME_J +3*(stream_index//self.FRAME_J) + 9*k_index]
        # generate the indices of the active part of the frame
        active_window = [(stream_index+ i%self.JAM_J + self.FRAME_J*(i//self.JAM_J)) for i in range(self.JAM_SIZE)]

        # Do the FMA's
        for i in range(self.JAM_SIZE): 
            ind = active_window[i]
            if k_index == 0: istream += [isa.fxmul(result[i], streams[ind], weight)]               
            elif k_index == 1: istream += [isa.fxcpmadd(result[i], weight, streams[ind], result[i])]
            elif k_index == 2: istream += [isa.fxcxma(result[i], weight, streams[ind], result[i])]
            
        return istream   
    
    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        return inst_str, com.c.execute(istream)

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        count = com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str, count
    
    def gen_header_defines(self):
        stencil_size = 7
        str = ''
        str+='#define GEN_KERNEL_A_OFFSET     (1)\n'
        str+='#define GEN_KERNEL_R_OFFSET     (1)\n'
        str+='#define GEN_KERNEL_NAME ("%s")\n' % self.kernel_name
        str+='#define GEN_KERNEL_OPERATOR_SIZE  (%.1f)\n' % stencil_size
        str+='#define GEN_KERNEL_LS_CYCLES (%.1f)\n' % ((self.FRAME_SIZE+self.JAM_SIZE+self.JAM_SIZE)*2)             # *2 cycles
        str+='#define GEN_KERNEL_FPU_CYCLES (%.1f)\n' % (stencil_size*self.JAM_SIZE)
        str+='#define GEN_KERNEL_NI_JAMS (%d)\n' % self.JAM_I
        str+='#define GEN_KERNEL_NJ_JAMS (%d)\n' % self.JAM_J
        str+='#define N_FLOPS (N_STENCILS*%.1f)\n' % (stencil_size*2-1)
            
        return str, 0
    
    def gen_initialize_registers(self):
        init_reg = ''
        
        # Reserve 16 integer registers for streams pointers
        # define the integer registers
        
        # define the pointers of the input data strides
        for i in range(self.frame_7pt.__len__()):
            init_reg += '    register double *a%d%d asm ("%d");\n' % \
            (self.frame_7pt[i]//self.FRAME_J, self.frame_7pt[i]%self.FRAME_J, self.gpr_pointers[i])
        init_reg += '\n'   

        for i in range(self.JAM_SIZE):     # define the pointers of the results strides
            init_reg += '    register double *r%d%d asm ("%d");\n' % \
            (i//self.JAM_J, i%self.JAM_J, self.gpr_pointers[i+self.gpr_results_offset])
        init_reg += '\n'   
         
        # Assign the input data strides' pointers to the integer registers
        shift = list(range(-1,self.FRAME_J-1))

        for i in self.frame_7pt:
            init_reg += \
            '    a%d%d = (double *) (a + %d*istride + %d*jstride - 1);\n' % \
            (i//self.FRAME_J, i%self.FRAME_J, shift[i//self.FRAME_J], shift[i%self.FRAME_J])    
        init_reg += '\n'
        
        # Assign the results strides pointers to the integer registers
        shift = list(range(self.JAM_J))
        for i in range(self.JAM_SIZE):
            init_reg += \
            '    r%d%d = (double *) (r + %d*istride + %d*jstride - 1);\n' % \
            (i//self.JAM_J, i%self.JAM_J, shift[i//self.JAM_J], shift[i%self.JAM_J])    
            
        # Assign the weights pointer to its integer register
        init_reg += '\n    weights = w - 2;\n'   
            
        return init_reg, 0

    def gen_prologue(self):
        com = self.Common(self)
        inst_str = ['']
        istream = []
        clock_count = [0]
                
        ## PROLOGUE
                
        # load weights registers
        istream += [isa.lfpdux(com.w_unique[i],com.w_p,com.sixteen) for i in range(4)]
        
        # load interior 1/2 input data-primary values for frame 1/3
        istream += [isa.lfsdux(com.streams[i],com.streams_p[i],com.eight) for i in self.interior_frame_7pt]
        istream += [isa.lfdux(com.streams[i],com.streams_p[i],com.eight) for i in self.interior_frame_7pt]
        
        self.generate_code_append(istream,com,inst_str,clock_count)       
        istream = []
        
        return inst_str[0],clock_count[0]
    
    def gen_inner_iter(self):        
        com = self.Common(self)
        istream = []

        for k in range(self.k_repeats):
            # do the FMA's for frame 1/3    
            istream += self.fma_block(com.w, com.streams, com.results, self.interior_block_ind, self.K0)
            
            # load for frame 2/3
            istream += [isa.lfpdux(com.streams[i],com.streams_p[i],com.sixteen) for i in self.exterior_frame_7pt]
            istream += [isa.lfsdux(com.streams[i],com.streams_p[i],com.eight) for i in self.interior_frame_7pt]

            # do the FMA's for frame 2/3    
            for i in self.block_ind: istream += self.fma_block(com.w, com.streams, com.results, i, self.K1)

	        # load exterior input data-pair values for frame 2/3
            istream += [isa.lfdux(com.streams[i],com.streams_p[i],com.eight) for i in self.interior_frame_7pt]        
            
            # do the FMA's for frame 3/3
            istream += self.fma_block(com.w, com.streams, com.results, self.interior_block_ind, self.K2)
            
            # write back                               result set 1/2
            istream += [isa.stfpdux(com.results[i],com.results_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
        
        return self.generate_code(istream,com)


