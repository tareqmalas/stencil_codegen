//-*- c -*-

#define GEN_KERNEL_NAME "Mock Kernel"
#define GEN_KERNEL_LS_CYCLES 1
#define GEN_KERNEL_FPU_CYCLES 1
#define GEN_KERNEL_NI_JAMS 2
#define GEN_KERNEL_NJ_JAMS 2
#define N_FLOPS (N_STENCILS*53.0)

#define kernel_reference    kernel_reference_27pt
#define fortran_kernel_     fortran_kernel_27_pt_

inline void gen_kernel(const double *restrict A, double *restrict R, const double *restrict W, int JK, int K)
{{
  {gen_initialize_registers!s};
  {gen_prologue!s};
  {gen_inner_iter!s};
  {gen_epilogue!s};
}}

