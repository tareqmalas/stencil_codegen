//-*- c -*-
/*
FP Registers allocation:
    0-15:   Input data frame (j is fast moving dimension)
    16-19:  First set of jam results
    20-23:  Second set of jam results
    24-31:  weights coefficients
    
    Weights mapping in the stencil operator
    frame  k=0     k=1     k=2
            j       j       j
          0 0 0   1 1 1   2 2 2
        i 0 0 0   1 1 1   2 2 2
          0 0 0   1 1 1   2 2 2
    
    Weight index   : 0  1  2  3  4  5  6  7
    Weight register: 24 25 26 27 28 29 30 31
*/
#define GEN_KERNEL_NAME ("{gen_kernel_name!s}")
#define GEN_KERNEL_OPERATOR_SIZE  (3.0)
#define GEN_KERNEL_LS_CYCLES ((8+8)*2)
#define GEN_KERNEL_FPU_CYCLES ((4*3)*2)
#define GEN_KERNEL_NI_JAMS (2)
#define GEN_KERNEL_NJ_JAMS (2)
#define N_FLOPS (N_STENCILS*5.0)

#define kernel_reference    kernel_reference_3pt
#define fortran_kernel_     fortran_kernel_3pt_

inline void gen_kernel(const double *restrict a, double *restrict r, const double *restrict w, int istride, int jstride) {{
//inline void gen_kernel(double *a, double *r, double *w, int istride, int jstride) {{
    // Allocate general purpose registers 
    // (do not touch 2, 14, or 30)
    register const double *restrict weights asm ("24");  
    register int zero asm ("25");
    register int eight asm ("26"); 
    register int sixteen asm ("27"); 
    register int k asm ("31");

    zero = 0; 
    eight = 8; // sizeof(double)
    sixteen = 16; // 8*2


//    asm(".__initialize_registers:");
{gen_initialize_registers!s}

//    asm(".__prologue:");
{gen_prologue!s}
//    asm(".__inner_iter:");
    for (k=2; k < jstride-3; k+=4) {{
{gen_inner_iter!s}
    }}

//    asm(".__epilogue:");
{gen_epilogue!s}
}}

