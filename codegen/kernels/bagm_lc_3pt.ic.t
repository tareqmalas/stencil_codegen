//-*- c -*-

{gen_header_defines!s}

#define kernel_reference    kernel_reference_3pt
#define fortran_kernel_     fortran_kernel_3pt_

inline void gen_kernel(const double *restrict a, double *restrict r, const double *restrict w, int istride, int jstride) {{
//inline void gen_kernel(double *a, double *r, double *w, int istride, int jstride) {{
    // Allocate general purpose registers 
    // (do not touch 2, 14, or 30)
    register const double *restrict weights asm ("26");  
//    register int zero asm ("25");
//    register int eight asm ("26"); 
    register int sixteen asm ("27"); 
    register int k asm ("31");

//    zero = 0; 
//    eight = 8; // sizeof(double)
    sixteen = 8; // start with 8 then change it to 16 later


//    asm(".__initialize_registers:");
{gen_initialize_registers!s}

//    asm(".__prologue:");
{gen_prologue!s}
//    asm(".__inner_iter:");
    for (k=2; k < jstride; k+=4) {{
{gen_inner_iter!s}
    }}
}}

