from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="1", verbose="1"):
        self.RESULTS_SET1 = 0
        self.RESULTS_SET2 = 1
        self.K0 = 0
        self.K1 = 1
        self.K2 = 2
        self.use_trace = verbose=="1"
        if interleave=="1":
            self.generate_code = self.print_scheduling
            self.kernel_name = "bagm-mute-3-point 2x2 jam Kernel"
        else:
            self.generate_code = self.print_no_scheduling
            self.kernel_name = "bagm-mute-3-point 2x2 jam Kernel - no interleaving"
                
    class Common:        
        def __init__(com_self, self):
            # create function to deal with blocks of registers 
            com_self.c = get_core(use_trace=self.use_trace)
            com_self.cv = CViewer()
            
            # Allocate fp registers 
            # Allocate 4 input data fp registers
            com_self.streams = com_self.c.acquire_fpregisters(range(4))
            # Allocate 8 results fp registers
            com_self.results = com_self.c.acquire_fpregisters(range(16,24))
            
            # Allocate 3 fp registers for the weights
            com_self.w = com_self.w_unique = com_self.c.acquire_fpregisters(range(24,27))            
            
            # Weights mapping to the 3-points stecil
            #    w ind: 0  1  2  
            #    w reg: 24 25 26
            #
            #frame   f0      f1      f2
            #       0 0 0   1 1 1   2 2 2
            #       0 0 0   1 1 1   2 2 2
            #       0 0 0   1 1 1   2 2 2
            #
            #       0  1  2    9  10 11    18 19 20
            #       3  4  5    12 13 14    21 22 23
            #       6  7  8    15 16 17    24 25 26
            #
#            com_self.w = [0]*27
#            
##            com_self.w[4+9*1] = com_self.w_unique[0]
##            com_self.w[3+9*1] = com_self.w[5+9*1] = com_self.w_unique[1]
##            com_self.w[1+9*1] = com_self.w[7+9*1] = com_self.w_unique[2]
##            com_self.w[4+9*0] = com_self.w[4+9*2] = com_self.w_unique[3]
##            com_self.w[1+9*0] = com_self.w[7+9*0] = com_self.w[1+9*2] = com_self.w[7+9*2] = com_self.w_unique[4]
##            com_self.w[3+9*0] = com_self.w[5+9*0] = com_self.w[3+9*2] = com_self.w[5+9*2] = com_self.w_unique[5]
##            com_self.w[0+9*1] = com_self.w[2+9*1] = com_self.w[6+9*1] = com_self.w[8+9*1] = com_self.w_unique[6]
##            com_self.w[0+9*0] = com_self.w[2+9*0] = com_self.w[6+9*0] = com_self.w[8+9*0] = \
##            com_self.w[0+9*2] = com_self.w[2+9*2] = com_self.w[6+9*2] = com_self.w[8+9*2] = com_self.w_unique[7]
#            com_self.w[0+9*0] = com_self.w[3+9*0] = com_self.w[6+9*0] = \
#            com_self.w[1+9*0] = com_self.w[4+9*0] = com_self.w[7+9*0] = \
#            com_self.w[2+9*0] = com_self.w[5+9*0] = com_self.w[8+9*0] = com_self.w_unique[0]
#            com_self.w[0+9*1] = com_self.w[3+9*1] = com_self.w[6+9*1] = \
#            com_self.w[1+9*1] = com_self.w[4+9*1] = com_self.w[7+9*1] = \
#            com_self.w[2+9*1] = com_self.w[5+9*1] = com_self.w[8+9*1] = com_self.w_unique[1]
#            com_self.w[0+9*2] = com_self.w[3+9*2] = com_self.w[6+9*2] = \
#            com_self.w[1+9*2] = com_self.w[4+9*2] = com_self.w[7+9*2] = \
#            com_self.w[2+9*2] = com_self.w[5+9*2] = com_self.w[8+9*2] = com_self.w_unique[2]
        
            # Allocate int registers
            # registers 14 and 30 are reserved, use at own risk
            # register 2 is causing error when used on Shaheen
            
            # Reserve integer registers for streams pointers
            # Input data streams pointers
            ind = list(range(3,11))
            streams_str = ['a%d%d' % (i//2,i%2) for i in range(4)]
            com_self.streams_p = [IntRegister(ind[i],streams_str[i]) for i in range(4)]
            # Results registers pointers
            ind = range(20,24)
            results_str = ['r%d%d' % (i//2,i%2) for i in range(4)]
            com_self.results_p = [IntRegister(ind[i],results_str[i]) for i in range(4)]
    
            # Weight pointer
            com_self.w_p = IntRegister(24,'weights')
            # constants
            com_self.zero = IntRegister(25,'zero')
            com_self.eight = IntRegister(26,'eight')
            com_self.sixteen = IntRegister(27,'sixteen')
            
#            # define the starting indices of the 2x2 sub-blocks
#            com_self.block_ind = [0,1,2,
#                              4,5,6,
#                              8,9,10]
                
    def fma_2x2block(self,w, stream, result, k_index, result_set):
        
        stream00 = stream[0]
        stream01 = stream[1]
        stream10 = stream[2]
        stream11 = stream[3]
        
        # Determine the current results set
        if result_set == 0:
            res00 = result[0]
            res01 = result[1]
            res10 = result[2]
            res11 = result[3]
        elif result_set == 1:
            res00 = result[4]
            res01 = result[5]
            res10 = result[6]
            res11 = result[7]
        else:
            raise Exception('result_set must be 0 or 1')                     

        # Determine the current weight coefficient
        weight = w[k_index]

        istream = []
        # Do the FMA's
        if k_index == 0:
            # no add in the first frame to initialize the results
            istream += [isa.fxmul(res00, stream00, weight)]
            istream += [isa.fxmul(res01, stream01, weight)]
            istream += [isa.fxmul(res10, stream10, weight)]
            istream += [isa.fxmul(res11, stream11, weight)]
        elif k_index == 1:
            istream += [isa.fxcpmadd(res00, weight, stream00,res00)]
            istream += [isa.fxcpmadd(res01, weight, stream01,res01)]
            istream += [isa.fxcpmadd(res10, weight, stream10,res10)]
            istream += [isa.fxcpmadd(res11, weight, stream11,res11)]
        elif k_index == 2:
            istream += [isa.fxcxma(res00, weight, stream00,res00)]
            istream += [isa.fxcxma(res01, weight, stream01,res01)]
            istream += [isa.fxcxma(res10, weight, stream10,res10)]
            istream += [isa.fxcxma(res11, weight, stream11,res11)]            
        else:
            raise Exception('k_index must be 0,1, or 2')
        
        return istream   
    
    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        istream = []
        return inst_str

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str
    
    def gen_kernel_name(self):
        return self.kernel_name
    
    def gen_initialize_registers(self):
        init_reg = ''
        
        # define the integer registers
        int_reg_ind = list(range(3,11))
        for i in range(4):     # define the pointers of the input data strides
            init_reg += '    register double *a%d%d asm ("%d");\n' % \
            (i//2, i%2, int_reg_ind[i])
        init_reg += '\n'   

        for i in range(4):     # define the pointers of the results strides
            init_reg += '    register double *r%d%d asm ("%d");\n' % \
            (i//2, i%2, int_reg_ind[4 + i])
        init_reg += '\n'
         
        # Assign the input data strides' pointers to the integer registers
        shift = [0, 1]
        for i in range(4):
            init_reg += \
            '    a%d%d = (double *) (a + %d*istride + %d*jstride - 2);\n' % \
            (i//2, i%2, shift[i//2], shift[i%2])    
        init_reg += '\n'   
        
        # Assign the results strides pointers to the integer registers
        for i in range(4):
            init_reg += \
            '    r%d%d = (double *) (r + %d*istride + %d*jstride - 1);\n' % \
            (i//2, i%2, shift[i//2], shift[i%2])    
            
        # Assign the weights pointer to its integer register
        init_reg += '\n    weights = w - 1;\n'   
            
        return init_reg

    def gen_prologue(self):
        com = self.Common(self)
        inst_str = ''
        istream = []
        
        ## PROLOGUE
        # load weights registers
        for i in range(3): istream += [isa.lfdux(com.w_unique[i],com.w_p,com.eight)]
        inst_str += self.generate_code(istream,com)
        istream = []
        inst_str += '\n    weights = w - 1;\n'
        for i in range(3): istream += [isa.lfsdux(com.w_unique[i],com.w_p,com.eight)]
        
        # load 4 input data-pair values for frame 1/3, result set 1/2
        istream += [isa.lfpdux(com.streams[i],com.streams_p[i],com.sixteen) for i in range(4)]
        # do the FMA's for              frame 1/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K0, self.RESULTS_SET1)

        inst_str += self.generate_code(istream,com)
        istream = []
 
         # shift the input data strides pointers by 1
        for i in range(4): 
            inst_str += '    a%d%d ++;\n' % (i//2, i%2)    
        inst_str += '\n'
        
        # load 4 input data values for frame 2/3, result set 1/2
        istream += [isa.lfdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
        inst_str += self.generate_code(istream,com)
        
        return inst_str
    
    def gen_inner_iter(self):        
        com = self.Common(self)
        istream = []

        ## ODD ITERATION
        # do the FMA's for              frame 2/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K1, self.RESULTS_SET1)
        # load 4 input data values for frame 3/3, result set 1/2 and 2/2
        istream += [isa.lfsdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
        # do the FMA's for              frame 3/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K2, self.RESULTS_SET1)
        # write back                               result set 1/2
        istream += [isa.stfxdux(com.results[i],com.results_p[i],com.sixteen) for i in range(0,4)]
        # do the FMA's for              frame 1/3, result set 2/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K0, self.RESULTS_SET2)
        
        ## EVEN ITERATION
        # load 4 input data values for frame 2/3, result set 2/2
        istream += [isa.lfdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
        # do the FMA's for              frame 2/3, result set 2/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K1, self.RESULTS_SET2)
        # load 4 input data values for frame 3/3, result set 2/2 and 1/2
        istream += [isa.lfsdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
        # do the FMA's for              frame 3/3, result set 2/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K2, self.RESULTS_SET2)
        # write back                               result set 2/2
        istream += [isa.stfxdux(com.results[i],com.results_p[i-4],com.sixteen) for i in range(4,8)]
        # do the FMA's for              frame 1/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K0, self.RESULTS_SET1)

        # load 4 input data values for frame 2/3, result set 1/2
        istream += [isa.lfdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
                        
        return self.generate_code(istream,com)

    def gen_epilogue(self):
        com = self.Common(self)
        istream = []

        ## EPILOGUE
        # do the FMA's for              frame 2/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K1, self.RESULTS_SET1)
        # load 4 input data values for frame 3/3, result set 1/2 and 2/2
        istream += [isa.lfsdux(com.streams[i],com.streams_p[i],com.eight) for i in range(4)]
        # do the FMA's for              frame 3/3, result set 1/2
        istream += self.fma_2x2block(com.w, com.streams, com.results, self.K2, self.RESULTS_SET1)
        # write back                               result set 1/2
        istream += [isa.stfxdux(com.results[i],com.results_p[i],com.sixteen) for i in range(0,4)]

        return self.generate_code(istream,com)

