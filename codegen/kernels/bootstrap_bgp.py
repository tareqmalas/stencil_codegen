from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile, PPC
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="0", verbose="0", unroll_i=2, unroll_j=2, k_repeats=1,no_fma=False):  
        self.max_fill = 12      
        self.no_fma = no_fma
        self.use_trace = 0 #verbose=="1"
        restricted_gpr = [0,1,2,14,30,  # do not use
                          11,12,13]        # stack backup/restore
        self.gpr_pointers = [i for i in range(32) if i not in restricted_gpr]
#        if interleave=="1":
#            self.generate_code = self.print_scheduling
#        else:
        self.generate_code = self.print_no_scheduling
            
        # create function to deal with blocks of registers 
        self.c = get_core(use_trace=self.use_trace, no_fma=self.no_fma)
        self.cv = CViewer(self.c)

        # Allocate the fp         str += 'register int k asm ("%d");\n' % self.loop_index.numregisters 
        self.fpr = self.c.acquire_fpregisters(range(32))
        self.fpr_pool = [0, 1, 2, 3, 4, 26, 27, 28, 29, 30]

        self.loop_index = IntRegister(self.gpr_pointers.pop(),'loop_index')
        self.sixteen1 = IntRegister(self.gpr_pointers.pop(),'sixteen1')
        self.sixteen2 = IntRegister(self.gpr_pointers.pop(),'sixteen2')
        self.gpr = [IntRegister(self.gpr_pointers.pop(),'gpr%d' % i) for i in range(7)]              

#        self.kernels_list = [self.kernel(self, isa.fxmul,isa.fxmul, 7, 'rar')]
        self.kernels_list = []
        for test in {isa.fxmul, isa.lfpdux, isa.stfpdux}:
            for depend in {isa.fxmul, isa.lfpdux, isa.stfpdux}:
                for hzd in {'waw', 'raw', 'war', 'rar', 'noh'}:
                    for fill in list(range(self.max_fill+1)):
                         k = self.kernel(self, test,depend, fill, hzd)
                         if k.is_valid(): self.kernels_list.append(k)
                                 
    def generate_code_append(self, istream, com, sched_stream, clock_count):
        sched, count = self.generate_code(istream,com)
        sched_stream[0] += sched
        clock_count[0] += count

    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        return inst_str, com.c.execute(istream)

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        count = com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str, count
        
    class kernel:
        def __init__(k_self, self, i_test_name, i_depend_name, fill_size, hazard='rar'):
            def extract_name_from_type(isa_type):
                '''from <class 'simasm.isa.XXX'> to XXX''' 
                str_type = str(isa_type)
                return str_type[19:-2]
            k_self.i_test_obj = i_test_name
            k_self.i_depend_obj = i_depend_name
            if fill_size > self.max_fill or fill_size < 0: 
                raise Exception('fill size must be in [0,%d]' % self.max_fill)
            else: k_self.fill_size = fill_size
            k_self.hazard = hazard            
            k_self.i_test_name = extract_name_from_type(k_self.i_test_obj)
            k_self.i_depend_name = extract_name_from_type(k_self.i_depend_obj)
                        
            k_self.assign_registers(self)

        def is_valid(k_self):
            if (k_self.i_depend_name== 'stfpdux' and k_self.hazard[0]=='w') or \
               (k_self.i_depend_name== 'lfpdux'  and k_self.hazard[0]=='r') or \
               (k_self.i_test_name  == 'stfpdux' and k_self.hazard[-1]=='w') or \
               (k_self.i_test_name  == 'lfpdux'  and k_self.hazard[-1]=='r'):
                return 0
            return 1
                
        def assign_registers(k_self,self):            
            ra1 = self.fpr[self.fpr_pool[0]]
            rc1 = self.fpr[self.fpr_pool[2]]
            rb1 = self.fpr[self.fpr_pool[1]]
            rt1 = self.fpr[self.fpr_pool[3]]
            
            rc2 = self.fpr[self.fpr_pool[6]]
            rb2 = self.fpr[self.fpr_pool[5]]

            if   k_self.hazard == 'waw': 
                ra2 = self.fpr[self.fpr_pool[4]]
                rt2 = rt1
            elif k_self.hazard == 'war': 
                ra2 = self.fpr[self.fpr_pool[4]]
                rt2 = ra1
            elif k_self.hazard == 'raw': 
                ra2 = rt1
                rt2 = self.fpr[self.fpr_pool[7]]
            elif k_self.hazard == 'rar': 
                ra2 = ra1
                rt2 = self.fpr[self.fpr_pool[7]]
            elif k_self.hazard == 'noh': 
                ra2 = self.fpr[self.fpr_pool[8]]
                rt2 = self.fpr[self.fpr_pool[9]]
#            Possibly add: rwar and rwaw (dependent FMA), or warw and rarw (test FMA)
            else:
                raise Exception('Hazard type must be one of: noh, waw, war, raw, or rar')

            if k_self.i_test_name == 'lfpdux': # we might want to use instruction without update indexed
                k_self.i_test = k_self.i_test_obj(rt1, self.gpr[0], self.gpr[1])
            elif  k_self.i_test_name == 'stfpdux':
                k_self.i_test = k_self.i_test_obj(ra1, self.gpr[0], self.gpr[1])
            elif  k_self.i_test_name == 'fxcxma':
                k_self.i_test = k_self.i_test_obj(rt1,ra1,rc1,rb1)
            elif  k_self.i_test_name == 'fxmul':
                k_self.i_test = k_self.i_test_obj(rt1,ra1,rc1)
            else:
                raise Exception('instruction must be of: {lfpdux,stfpdux,fxcxma,fxmul}. Given: '+ k_self.i_test_name)            
            
            if k_self.i_depend_name == 'lfpdux': # we might want to use instruction without update indexed
                k_self.i_depend = k_self.i_depend_obj(rt2, self.gpr[2], self.gpr[3])
            elif  k_self.i_depend_name == 'stfpdux':
                k_self.i_depend = k_self.i_depend_obj(ra2, self.gpr[2], self.gpr[3])
            elif  k_self.i_depend_name == 'fxcxma':
                k_self.i_depend = k_self.i_depend_obj(rt2,ra2,rc2,rb2)
            elif  k_self.i_depend_name == 'fxmul':
                k_self.i_depend = k_self.i_depend_obj(rt2,ra2,rc2)
            else:
                raise Exception('instruction must be of: {lfpdux,stfpdux,fxcxma,fxmul}')
                
        def c_func_name(k_self):
            return k_self.i_test_name +"_"+\
                k_self.i_depend_name  +"_"+\
                k_self.hazard +"_"+ str(k_self.fill_size)

        def c_struct_str(k_self):
            return '{"%s", %s, %d, "%s", "%s", "%s"}' \
                % (k_self.c_func_name(),k_self.c_func_name(),k_self.fill_size,k_self.hazard,k_self.i_test_name,k_self.i_depend_name)
            
    def gen_kernels_struct(self):
        struct_list = ',\n'.join([self.kernels_list[i].c_struct_str() for i in range(len(self.kernels_list))])                
        return struct_list, 0
    
    def generate_one_kernel(self, com, k_index):
        def fill_generate(size): 
            return [isa.fadd(com.fpr[i],com.fpr[i+1],com.fpr[i+2]) for i in range(5,5+size*3,3)] 
        c_kernel = ['']
        istream = []
        clock_count = [0]
        
        c_kernel[0] += 'inline void %s() {\n' % com.kernels_list[k_index].c_func_name()
        c_kernel[0] += '  %s = dummy1;\n' % self.gpr[0].c_var      
        c_kernel[0] += '  %s = 0;\n'      % self.gpr[1].c_var
        c_kernel[0] += '  %s = dummy2;\n' % self.gpr[2].c_var
        c_kernel[0] += '  %s = 0;\n'      % self.gpr[3].c_var
        c_kernel[0] += '  %s = dummy3;\n' % self.gpr[4].c_var
        c_kernel[0] += '  %s = 0;\n'      % self.gpr[5].c_var
      
        c_kernel[0] += '  for(k=0; k<N_CYCLES; k++) {\n'        
        
        ifill = fill_generate(6) + fill_generate(6)
        
        if com.kernels_list[k_index].i_test.unit == PPC.FP:
            istream += [isa.fadd(self.fpr[23], self.fpr[24], self.fpr[25])]
        else:
            istream += [isa.stfpdux(self.fpr[25], self.gpr[4], self.gpr[5])]    
        istream += [com.kernels_list[k_index].i_test]
        
        fill_size = com.kernels_list[k_index].fill_size
#        istream += ifill[0:fill_size] + [com.kernels_list[k_index].i_depend] + ifill[fill_size:-(6-fill_size)] + [isa.nop()]*5
        istream += ifill[0:fill_size] + [com.kernels_list[k_index].i_depend] + ifill[fill_size:] + [isa.nop()]*10
                
        self.generate_code_append(istream,com,c_kernel,clock_count)       
        c_kernel[0] += '  }\n}\n\n'        
        return c_kernel[0]
    
    def gen_kernels_func(self):
        all_funcs = ''        
 
        for i in range(len(self.kernels_list)):
            all_funcs += self.generate_one_kernel(self,i)
        
        return all_funcs, 0

    def gen_header_defines(self):
        str = ''
        str += '#define N_KERNELS     (%d)\n' % len(self.kernels_list)
        str += '\n'
        str += 'volatile double dummy1[2] = {23.234, 54.344};\n'
        str += 'volatile double dummy2[2] = {23.234, 54.344};\n'
        str += 'volatile double dummy3[2] = {25.234, 45.344};\n'
        str += 'register int k asm ("%d");\n' % self.loop_index.num
        str += 'register volatile double * %s asm ("%d");\n' % (self.gpr[0].c_var,self.gpr[0].num)
        str += 'register int %s asm ("%d");\n' % (self.gpr[1].c_var,self.gpr[1].num)
        str += 'register volatile double * %s asm ("%d");\n' % (self.gpr[2].c_var,self.gpr[2].num)
        str += 'register int %s asm ("%d");\n' % (self.gpr[3].c_var,self.gpr[3].num)
        str += 'register volatile double * %s asm ("%d");\n' % (self.gpr[4].c_var,self.gpr[4].num)
        str += 'register int %s asm ("%d");\n' % (self.gpr[5].c_var,self.gpr[5].num)

        str += '\n#define MAX_FILL (%d)\n' % self.max_fill
        return str, 0

