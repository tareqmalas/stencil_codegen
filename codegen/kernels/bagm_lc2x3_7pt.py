"""
7-point stencil steps:
=======================================
            PROLOGUE
a1   ← A[i,j,k+0]      ;load to get ax0    
a2   ← A[i,j,k+1:k+2]  ;load to get a12  int  
a    ← A[i,j,k+1:k+2]  ;load to get a12  ext  
a1.p  = a2.p           ;copy to get a10    

=======================================
            LOOP
r12   = a1 '* W0
a1  ← A[i,j,k+3:k+4]   ;load to get a34  int  
r12  += a2  * W1
a   ← A[i,j,k+3:k+4]   ;load to get a34  ext  
a2.p  = a1.p           ;copy to get a32    
r12  += a2 '* W2
r12 → R[i,j,k+1:k+2]   
---------------------------------------
r34   = a2 '* W0                         
a2    ← A[i,j,k+5:k+6] ;load to get a56  int
r34  += a1  * W1
a     ← A[i,j,k+5:k+6] ;load to get a56  ext
a1.p  = a2.p           ;copy to get a54  
r34  += a1 '* W2
r34 → R[i,j,k+3:k+4]   

=======================================
            EPILOGUE 
NONE

            
#r56   = a1 '* W0
#a1    ← A[i,j,k+7:k+8]  ;load to get a78
#r56  += a2  * W1
#a2.p  = a1.p            ;copy to get a76
#r56  += a2 '* W2
#r56 → R[i,j,k+5:k+6]
"""

from simasm.view import CViewer
from simasm import isa
from simasm.ppc import Register, FPRegister, IntRegister, RegisterFile
import itertools
from collections import OrderedDict, deque, defaultdict
from simasm.simulate import Core, get_core

class Generator:

    def __init__(self, interleave="1", verbose="1", unroll_i=2, unroll_j=3, k_repeats=1,no_fma=False):
        self.no_fma = no_fma
        if unroll_i != 2 or unroll_j != 3: raise Exception('This kernel requires unroll_i=2, unroll_j=3')
        self.JAM_I = 2
        self.JAM_J = 3

        self.k_repeats = k_repeats        
        
        self.FRAME_I = self.JAM_I+2
        self.FRAME_J = self.JAM_J+2
        self.JAM_SIZE = self.JAM_I * self.JAM_J
        self.FRAME_SIZE = self.FRAME_I * self.FRAME_J

        self.K0 = 0
        self.K1 = 1
        self.K2 = 2
        self.use_trace = verbose=="1"
        
        self.frame_7pt = list(range(self.FRAME_SIZE))
        del self.frame_7pt[self.FRAME_SIZE-1]
        del self.frame_7pt[self.FRAME_SIZE-self.FRAME_J]
        del self.frame_7pt[self.FRAME_J-1]
        del self.frame_7pt[0]        

        self.block_ind = [1, self.FRAME_J, self.FRAME_J+1, self.FRAME_J+2, 2*self.FRAME_J+1]
#        self.block_ind = [    1,
#                          5 , 6, 7,
#                             11]
        self.interior_block_ind = self.FRAME_J+1

        # define the starting indices of the 2x3 sub-blocks
        #        *   1   *   *   *
        #        5   6   7   *   *
        #        *   11  *   *   *
        #        *   *   *   *   *
        self.interior_frame_7pt = [(self.interior_block_ind + i%self.JAM_J + self.FRAME_J*(i//self.JAM_J)) for i in range(self.JAM_SIZE)]
#        self.interior_frame_7pt =[    
#                                     6 ,7 ,8 , 
#                                     11,12,13
#                                                ]
        self.exterior_frame_7pt = list(set(self.frame_7pt).difference(set(self.interior_frame_7pt)))
#        self.exterior_frame_7pt =[   1 ,2 ,3 ,    
#                                  5 ,         9 , 
#                                  10,         14,
#                                     16,17,18  ]
        
            
        self.streams_GPR_pointers = list(range(3,13)) + [29] + list(range(15,20))
        self.results_GPR_pointers = list(range(20,26))
        
        k_name = "bagm-load-copy-7-point 2x3 jam Kernel"
        if interleave=="1":
            self.generate_code = self.print_scheduling
            self.kernel_name = k_name
        else:
            self.generate_code = self.print_no_scheduling
            self.kernel_name = k_name + " - no interleaving"
        if no_fma:
            self.kernel_name = k_name + " - FMA OPS DISABLED"
    def generate_code_append(self, istream, com, sched_stream, clock_count):
        sched, count = self.generate_code(istream,com)
        sched_stream[0] += sched
        clock_count[0] += count
        
    class Common:        
        def __init__(com_self, self):
            # create function to deal with blocks of registers 
            com_self.c = get_core(use_trace=self.use_trace, no_fma=self.no_fma)
            com_self.cv = CViewer(com_self.c)
            
            # Allocate fp registers 
            FPR_pool = com_self.c.acquire_fpregisters(range(27,-1,-1))
            # preserve the indexing of 4x5 block by allocating dummy indices in the frame 
            # Allocate 10 input data fp registers (0-9) for the exterior of the frame 
            com_self.exterior_streams = [0]*self.FRAME_SIZE
            for i in range(self.exterior_frame_7pt.__len__()): com_self.exterior_streams[self.exterior_frame_7pt[i]] = FPR_pool.pop()
            # Allocate another 6x2 input data fp registers (10-15, 16-21) for interior streams
            com_self.interior_streams1 = [0]*self.FRAME_SIZE
            for i in range(self.JAM_SIZE): com_self.interior_streams1[self.interior_frame_7pt[i]] = FPR_pool.pop()
            com_self.interior_streams2 = [0]*self.FRAME_SIZE
            for i in range(self.JAM_SIZE): com_self.interior_streams2[self.interior_frame_7pt[i]] = FPR_pool.pop()            
            # Allocate 6 results fp registers (22-27)
            com_self.results = [0]*self.JAM_SIZE
            for i in range(self.JAM_SIZE): com_self.results[i] = FPR_pool.pop()            
            
            # Allocate 4 fp registers for the weights
            com_self.w_unique = com_self.c.acquire_fpregisters(range(28,32))            
            
            # Weights mapping to the 7-points stecil
            #    w ind: 0  1  2  3
            #    w reg: 28 29 30 31
            #
            #frame   f0      f1      f2
            #                 2
            #         3     1 0 1     3
            #                 2
            #
            #       0  1  2    9  10 11    18 19 20
            #       3  4  5    12 13 14    21 22 23
            #       6  7  8    15 16 17    24 25 26
            #
            # reserve 3x3x3 indexing for the 4 weights
            com_self.w = [0]*27
            
            com_self.w[4+9*1] = com_self.w_unique[0]
            com_self.w[3+9*1] = com_self.w[5+9*1] = com_self.w_unique[1]
            com_self.w[1+9*1] = com_self.w[7+9*1] = com_self.w_unique[2]
            com_self.w[4+9*0] = com_self.w[4+9*2] = com_self.w_unique[3]
        
            # Allocate int registers
            # registers 14 and 30 are reserved, use at own risk
            # register 2 is causing error when used on Shaheen
            
            # Reserve 20 integer registers for streams pointers (corners are dummy to ease the indexing)
            # Input data streams pointers
            streams_str = ['a%d%d' % (i//self.FRAME_J,i%self.FRAME_J) for i in range(self.FRAME_SIZE)]
            com_self.streams_p = [0]*self.FRAME_SIZE
            for i in range(self.frame_7pt.__len__()): com_self.streams_p[self.frame_7pt[i]] = \
                IntRegister(self.streams_GPR_pointers[i],streams_str[self.frame_7pt[i]])
            # Results registers pointers
            results_str = ['r%d%d' % (i//self.JAM_J,i%self.JAM_J) for i in range(self.JAM_SIZE)]
            com_self.results_p = [IntRegister(self.results_GPR_pointers[i],results_str[i]) for i in range(self.JAM_SIZE)]
    
            # constants
#            com_self.zero = IntRegister(24,'zero')
#            com_self.eight = IntRegister(25,'eight')
            com_self.sixteen = IntRegister(27,'sixteen')
            # Weight pointer
            com_self.w_p = IntRegister(26,'weights')

    def fma_block(self,w, exterior_streams, interior_streams, result, stream_index, k_index):
        istream = []
        # Determine the current weight coefficient
        weight = w[stream_index%self.FRAME_J +3*(stream_index//self.FRAME_J) + 9*k_index]
        # generate the indices of the active part of the frame
        active_window = [(stream_index+ i%self.JAM_J + self.FRAME_J*(i//self.JAM_J)) for i in range(self.JAM_SIZE)]

        # Do the FMA's
        for i in range(self.JAM_SIZE): 
            ind = active_window[i]
            if k_index == 0: istream += [isa.fxmul(result[i], interior_streams[ind], weight)]               
            elif k_index == 1:
                if ind in self.exterior_frame_7pt: istream += [isa.fxcpmadd(result[i], weight, exterior_streams[ind], result[i])]
                elif ind in self.interior_frame_7pt: istream += [isa.fxcpmadd(result[i], weight, interior_streams[ind], result[i])]
            elif k_index == 2: istream += [isa.fxcxma(result[i], weight, interior_streams[ind], result[i])]
#            else:
#                raise Exception('k_index must be 0,1, or 2')
            
        return istream   
    
    def print_no_scheduling(self,istream,com):
        inst_str = ''
        inst_str += '\n'.join([com.cv.named_view(i) for i in istream])
        inst_str += '\n'
        return inst_str, com.c.execute(istream)

    def print_scheduling(self,istream,com):
        inst_str = ''
        com.c.inline_asm = ''
        count = com.c.schedule(istream)
        inst_str += com.c.inline_asm
        inst_str += '\n'
        return inst_str, count
    
    def gen_kernel_name(self):
        return self.kernel_name, 0
    
    def gen_initialize_registers(self):
        init_reg = ''
        
        # Reserve 16 integer registers for streams pointers
        # define the integer registers
        
        # define the pointers of the input data strides
        for i in range(self.frame_7pt.__len__()):
            init_reg += '    register double *a%d%d asm ("%d");\n' % \
            (self.frame_7pt[i]//self.FRAME_J, self.frame_7pt[i]%self.FRAME_J, self.streams_GPR_pointers[i])
        init_reg += '\n'   

        for i in range(self.JAM_SIZE):     # define the pointers of the results strides
            init_reg += '    register double *r%d%d asm ("%d");\n' % \
            (i//self.JAM_J, i%self.JAM_J, self.results_GPR_pointers[i])
        init_reg += '\n'   
         
        # Assign the input data strides' pointers to the integer registers
        shift = list(range(-1,self.FRAME_J-1))

        for i in self.interior_frame_7pt:
            init_reg += \
            '    a%d%d = (double *) (a + %d*istride + %d*jstride - 1);\n' % \
            (i//self.FRAME_J, i%self.FRAME_J, shift[i//self.FRAME_J], shift[i%self.FRAME_J])    
        init_reg += '\n'   
        for i in self.exterior_frame_7pt:
            init_reg += \
            '    a%d%d = (double *) (a + %d*istride + %d*jstride - 1);\n' % \
            (i//self.FRAME_J, i%self.FRAME_J, shift[i//self.FRAME_J], shift[i%self.FRAME_J])    
        init_reg += '\n'   
        
        # Assign the results strides pointers to the integer registers
        shift = list(range(self.JAM_J))
        for i in range(self.JAM_SIZE):
            init_reg += \
            '    r%d%d = (double *) (r + %d*istride + %d*jstride - 1);\n' % \
            (i//self.JAM_J, i%self.JAM_J, shift[i//self.JAM_J], shift[i%self.JAM_J])    
            
        # Assign the weights pointer to its integer register
        init_reg += '\n    weights = w - 2;\n'   
            
        return init_reg, 0

    def gen_prologue(self):
        com = self.Common(self)
        inst_str = ['']
        istream = []
        clock_count = [0]
                
        ## PROLOGUE
        
        # load 6 interior 1/2 input data-primary values for frame 1/3
        istream += [isa.lfsdux(com.interior_streams1[i],com.streams_p[i],com.sixteen) for i in self.interior_frame_7pt]
        
        self.generate_code_append(istream,com,inst_str,clock_count)       
        istream = []

        # do pointer shift
        for i in self.interior_frame_7pt:
            inst_str[0] += '    a%d%d --;\n' % (i//5, i%5)    
        inst_str[0] += '\n'
        # change the constant from 8 to 16
        inst_str[0] += '    sixteen = 16;\n\n'
        
        # load weights registers
        for i in range(4): istream += [isa.lfpdux(com.w_unique[i],com.w_p,com.sixteen)]
        
        # load 6 interior 2/2 input data-pair values to prepare frame 2/3
        istream += [isa.lfpdux(com.interior_streams2[i],com.streams_p[i],com.sixteen) for i in self.interior_frame_7pt]
        # load 10 exterior input data-pair values for frame 2/3
        istream += [isa.lfpdux(com.exterior_streams[i],com.streams_p[i],com.sixteen) for i in self.exterior_frame_7pt]
        
        # copy the interior primary from frame 2 to frame 1
        istream += [isa.fmr(com.interior_streams1[i],com.interior_streams2[i]) for i in self.interior_frame_7pt]
 
#        # we can move some FMA's here to make use of some stalled cycles:
#        # do the FMA's for              frame 1/3
#        istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams1, com.results, self.interior_block_ind, self.K0)
#        # do the FMA's for              frame 2/3
#        for i in self.block_ind: istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams2, com.results, i, self.K1)

        self.generate_code_append(istream,com,inst_str,clock_count)       
        istream = []
        
        return inst_str[0],clock_count[0]
    
    def gen_inner_iter(self):        
        com = self.Common(self)
        istream = []

        for k in range(self.k_repeats):
            ## ODD ITERATION
            # do the FMA's for              frame 1/3
            istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams1, com.results, self.interior_block_ind, self.K0)
            
            # load 6 interior 1/2 input data-pair values to prepare frame 3/3
            istream += [isa.lfpdux(com.interior_streams1[i],com.streams_p[i],com.sixteen) for i in self.interior_frame_7pt]
            
            # do the FMA's for              frame 2/3
            for i in self.block_ind: istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams2, com.results, i, self.K1)

        	# load 10 exterior input data-pair values for frame 2/3
            istream += [isa.lfpdux(com.exterior_streams[i],com.streams_p[i],com.sixteen) for i in self.exterior_frame_7pt]
            
            # copy the interior primary from frame 1 to frame 2 for frame 3/3
            istream += [isa.fmr(com.interior_streams2[i],com.interior_streams1[i]) for i in self.interior_frame_7pt]

            # do the FMA's for              frame 3/3
            istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams2, com.results, self.interior_block_ind, self.K2)
            
            # write back                               result set 1/2
            istream += [isa.stfpdux(com.results[i],com.results_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
            
            
            ## EVEN ITERATION
            # do the FMA's for              frame 1/3
            istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams2, com.results, self.interior_block_ind, self.K0)
            
            # load 6 interior 1/2 input data-pair values to prepare frame 3/3
            istream += [isa.lfpdux(com.interior_streams2[i],com.streams_p[i],com.sixteen) for i in self.interior_frame_7pt]
            
            # do the FMA's for              frame 2/3
            for i in self.block_ind: istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams1, com.results, i, self.K1)
	
	        # load 10 exterior input data-pair values for frame 2/3
            istream += [isa.lfpdux(com.exterior_streams[i],com.streams_p[i],com.sixteen) for i in self.exterior_frame_7pt]
            
            # copy the interior primary from frame 1 to frame 2 for frame 3/3
            istream += [isa.fmr(com.interior_streams1[i],com.interior_streams2[i]) for i in self.interior_frame_7pt]

            # do the FMA's for              frame 3/3
            istream += self.fma_block(com.w, com.exterior_streams, com.interior_streams1, com.results, self.interior_block_ind, self.K2)
            
            # write back                               result set 1/2
            istream += [isa.stfpdux(com.results[i],com.results_p[i],com.sixteen) for i in range(self.JAM_SIZE)]
        
        
        return self.generate_code(istream,com)


