include ../make.inc

PYTHON_BUILDER = ./build.py

BUILD_DIR=build
MUTANT_KERNEL  = bagm_mute_27pt
MUTANT_DRIVER_OPTS =
MUTANT_BUILDER_OPTS=-v 0 -i 1 --build_dir ${BUILD_DIR}

BS_BUILD_DIR=bs_build
BS_KERNEL  = bootstrap_bgp
BS_DRIVER_OPTS =
BS_BUILDER_OPTS=-v 0 -i 0 --build_dir ${BS_BUILD_DIR}

.PHONY: all 

all: ${BUILD_DIR} ${BUILD_DIR}/${MUTANT_KERNEL}
all_bs: ${BS_BUILD_DIR} ${BS_BUILD_DIR}/${BS_KERNEL}

simasm:
	test -d simasm || git clone git://github.com/jedbrown/simasm.git

${BUILD_DIR}:
	mkdir -p ${BUILD_DIR}

${BS_BUILD_DIR}:
	mkdir -p ${BS_BUILD_DIR}

${BUILD_DIR}/fortran_kernel.o: drivers/fortran_kernel.F90
	${FC} ${FFLAGS} -o $@ -c $<

${BUILD_DIR}/gen_kernel.ic: kernels/${MUTANT_KERNEL}.py kernels/${MUTANT_KERNEL}.ic.t simasm
	${PYTHON_BUILDER} -k ${MUTANT_KERNEL} ${MUTANT_BUILDER_OPTS}

${BUILD_DIR}/c_kernel.o: drivers/c_kernel.c ${BUILD_DIR}/gen_kernel.ic
	${CC} ${CFLAGS} -I${BUILD_DIR} -o $@ -c $< ${MUTANT_DRIVER_OPTS}

${BUILD_DIR}/driver.o: drivers/driver.c ${BUILD_DIR}/gen_kernel.ic
	${CC} ${CFLAGS} -I${BUILD_DIR} -o $@ -c $< ${MUTANT_DRIVER_OPTS} -qnosmp

${BUILD_DIR}/${MUTANT_KERNEL}: ${BUILD_DIR}/driver.o ${BUILD_DIR}/c_kernel.o ${BUILD_DIR}/fortran_kernel.o
	${CC} ${CFLAGS} -o $@ $^ ${LDFLAGS} ${LIBS}

${BS_BUILD_DIR}/gen_kernel.ic: kernels/${BS_KERNEL}.py kernels/${BS_KERNEL}.ic.t simasm
	${PYTHON_BUILDER} -k ${BS_KERNEL} ${BS_BUILDER_OPTS}

${BS_BUILD_DIR}/bootstrap_bgp_driver.o: drivers/bootstrap_bgp_driver.c ${BS_BUILD_DIR}/gen_kernel.ic
	${CC} ${CFLAGS} -I${BS_BUILD_DIR} -o $@ -c $< ${BS_DRIVER_OPTS} -qnosmp

${BS_BUILD_DIR}/${BS_KERNEL}: ${BS_BUILD_DIR}/bootstrap_bgp_driver.o
	${CC} ${CFLAGS} -o $@ $^ ${LDFLAGS} ${LIBS}

clean:
	-rm -f ${BUILD_DIR}/gen_kernel.ic ${BUILD_DIR}/${MUTANT_KERNEL} ${BUILD_DIR}/*.o ${BS_BUILD_DIR}/gen_kernel.ic ${BS_BUILD_DIR}/${BS_KERNEL} ${BS_BUILD_DIR}/*.o

clean_all:
	-rm -rf ${BUILD_DIR} ${BS_BUILD_DIR}
