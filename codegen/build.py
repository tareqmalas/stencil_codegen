#!/usr/bin/env python3


def main():
    args = parse_args()
    
    if not args.dry_run:
        build_dir = './'+args.build_dir
        ensure_dir(build_dir)

    gen_params = {'interleave':args.interleave, 'verbose':args.verbose}
    
    opt_params = ['unroll_i', 'unroll_j', 'k_repeats', 'no_fma']
    for k in opt_params: 
        if getattr(args,k) is not None:
            gen_params[k] =  getattr(args,k)
    template_dict   = get_template_dict(args.kernel, gen_params)
    if not args.dry_run:
        kernel_template = get_template(args.kernel)
        write_template(args.kernel, kernel_template, template_dict, build_dir)
        build(build_dir)

def get_template(kernel):
    import os
    with open(os.path.join('kernels',kernel+'.ic.t')) as template_file:
        kernel_template = template_file.read()
    return kernel_template
    
def write_template(kernel, kernel_template, template_dict, build_dir):
    import os
    with open(os.path.join(build_dir,'gen_kernel.ic'),'w') as kernel_file:
        kernel_file.write(kernel_template.format(**template_dict))

def build(build_dir):
    pass

def get_template_dict(kernel, gen_params):
    import os, importlib, kernels, inspect

    kernel_module = importlib.import_module('kernels.'+kernel,'kernels')

    generators = [m for m in inspect.getmembers(kernel_module.Generator, inspect.isfunction) 
                  if m[0][0:4] == 'gen_']
        
    g = kernel_module.Generator(**gen_params)
    template_dict = dict()
    for name, method in generators:
        inst, clock = method(g)
        print(name+': '+str(clock))
        template_dict[name] = inst
    return template_dict
    
def parse_args():
    import argparse
    import sys
    from glob import glob
    from os.path import basename

    parser = argparse.ArgumentParser(description='Build.py: Generates kernels')
    parser.add_argument('-k','--kernel', default='mock',
                        help='kernel to generate')

    # arguments for the kernel's Python generator
    parser.add_argument('-i','--interleave', 
                        help='enable/disable instructions interleaving (default 1 "enabled")')
    parser.add_argument('-v','--verbose', default="1", help='Show the log of the simulator (default 1 "enabled")')
    parser.add_argument('-n','--dry-run', help='Do not write the generated kernel', action='store_true')
    parser.add_argument('--unroll_j', type=int, help='specify the unrolling size in J')
    parser.add_argument('--unroll_i', type=int, help='specify the unrolling size in I')
    parser.add_argument('--k_repeats', type=int, help='hacky unrolling in k, do not use for production code')
    parser.add_argument('--no_fma', type=int, help='disable the FMAs generation in the code')
    parser.add_argument('-d','--build_dir', default="build", help='build directory name (default "build")')

    class ListAction(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            print('[Kernels]\n' + '\n'.join(basename(k).replace('.ic.t','') for k in glob('kernels/*ic.t')))
            sys.exit(0)
    parser.add_argument('-l','--list',action=ListAction, nargs=0,
                        help='list available kernels')
    
    return parser.parse_args()

def ensure_dir(d):
    import os, errno
    try:
        os.makedirs(d)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else: raise

if __name__ == "__main__":
    main()
