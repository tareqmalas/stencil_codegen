#ifndef N_I
#define N_I (GEN_KERNEL_NI_JAMS + 2)     
#endif

#ifndef N_J
#define N_J (GEN_KERNEL_NJ_JAMS + 2)    
#endif

#ifndef N_K
#define N_K (198)   
#endif

#ifndef ALIGNMENT
#define ALIGNMENT (16)
#endif

#ifndef N_TESTS
#define N_TESTS (10)
#endif

#ifndef N_CYCLES
#define N_CYCLES (1000)
#endif

#ifndef PRINT_INFO
#define PRINT_INFO (1)
#endif

#ifndef PRINT_NODE_INFO
#define PRINT_NODE_INFO (1)
#endif

#ifndef PRINT_PERFORMANCE
#define PRINT_PERFORMANCE (1)
#endif

#ifndef PRINT_EFFICIENCY
#define PRINT_EFFICIENCY (1)
#endif

#ifndef VERIFY
#define VERIFY (1)
#endif

#ifndef USE_FORTRAN_KERNEL
#define USE_FORTRAN_KERNEL (0)
#endif

#ifndef USE_C_KERNEL
#define USE_C_KERNEL (0)
#endif

#ifndef FLUSH_ALL_CACHES
#define FLUSH_ALL_CACHES (0)
#endif    

#ifndef FLUSH_L1_CACHE
#define FLUSH_L1_CACHE (0)
#endif

#ifndef TOUCH_L1
#define TOUCH_L1 (0)
#endif

#ifndef PRINT_TIME_DETAILS
#define PRINT_TIME_DETAILS (0)
#endif
