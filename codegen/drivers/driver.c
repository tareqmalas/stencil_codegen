//driver.c
//main routine for testing 27-point stencil kernels

#define _XOPEN_SOURCE 600       /* make stdlib.h provide posix_memalign() in strict C99 mode */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include "mpi.h"
#include "gen_kernel.ic"
#include "driver_defaults.h"

#define DOMAIN_SIZE (N_I*N_J*N_K)

#define N_STENCILS ((N_I-2)*(N_J-2)*(N_K-2))

#define N_TICKS (850*1000*1000)

#define N_FMA (N_FLOPS/2)

#if USE_FORTRAN_KERNEL
#  undef GEN_KERNEL_NAME
#  define GEN_KERNEL_NAME "Fortran Reference"
extern void fortran_kernel_(const double *restrict,double *restrict,const double *restrict,const int*,const int*,const int*);
static inline void kernel_wrap(const double *restrict A,double *restrict R,const double *restrict W)
{
  int ni = N_I,nj = N_J,nk = N_K;
  fortran_kernel_(A,R,W,&ni,&nj,&nk);
}
#elif USE_C_KERNEL
#  undef GEN_KERNEL_NAME
#  define GEN_KERNEL_NAME "C Reference"
#  define kernel_wrap kernel_reference
#else
// this and all GEN_KERNEL_* macros available from gen_kernel.ic
static void kernel_wrap(const double *restrict A,double *restrict R, const double *restrict W)
{
    complex double Mystack[19];
    int i, j;
  
    register complex double *k asm ("31");
    k = Mystack - 1;

    register int sixteen asm ("27"); 
    sixteen = 16; // 8*2

    asm volatile ("stfpdux 13, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 14, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 15, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 16, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 17, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 18, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 19, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 20, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 21, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 22, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 23, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 24, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 25, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 26, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 27, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 28, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 29, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 30, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("stfpdux 31, %0, %1":"+b" (k):"r" (sixteen));
  
    for (i=1; i<N_I-1; i+=GEN_KERNEL_NI_JAMS) {
        for (j=1; j<N_J-1; j+=GEN_KERNEL_NJ_JAMS) {
            gen_kernel(&A[(i*N_J+j)*N_K], &R[(i*N_J+j)*N_K], W, N_J*N_K, N_K);
        }
    }

    k = Mystack - 1;
    asm volatile ("lfpdux 13, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 14, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 15, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 16, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 17, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 18, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 19, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 20, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 21, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 22, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 23, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 24, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 25, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 26, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 27, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 28, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 29, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 30, %0, %1":"+b" (k):"r" (sixteen));
    asm volatile ("lfpdux 31, %0, %1":"+b" (k):"r" (sixteen));

}
#endif

extern void kernel_reference(const double *restrict ,double *restrict ,const double *restrict );
void print_3Darray(char *filename, double *array);

// cribbed from LLNL example here: https://computing.llnl.gov/tutorials/openMP/samples/C/omp_mm.c

// 8*1024 doubles is 64KB
// 1,000*1024 doubles is ~ 8MB, taking the sum of these is more than enough to flush the caches
int mpi_rank, mpi_size;
#define KB (1024)
#define MB (1024*1024)
#define GB (1024*1024*1024)
#define L1_BIG (8*1024)
#define L3_BIG (1000*1024)
double l1_big[L1_BIG];
double l3_big[L3_BIG];
double touch_half_l1(double * array, int size)
{
	int i;
	double xx = 0;
	for (i=0;(i<(((32*1024)/8)/2) && (i<size)); i++) {
		xx += array[i];
	}
	if (mpi_size<-1) printf("%1.0f",xx);
	return xx;
}
double flush_l1()
{
	int i;
	double xx = 0;
	for (i=0;i<L1_BIG; i++) {
		xx += l1_big[i];
	}
	if (mpi_size<-1) printf("%1.0f",xx);	
	return xx;
}
double flush_caches()
{
	int i;
	double xx = 0;
	for (i=0;i<L3_BIG; i++) {
		xx += l3_big[i];
	}
	if (mpi_size<-1) printf("%1.0f",xx);
	return xx;
}

int main (int argc, char *argv[]) 
{
    int i, j, tests_remain;
    double t_start, t_end, t;

    double * W;
    double * A;
    double * R;

    MPI_Init(&argc,&argv); 
    MPI_Comm_rank (MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);

    // verify the domain size correctness
    if (mpi_rank ==0) {
      if (GEN_KERNEL_OPERATOR_SIZE==27){
        if (N_K%2 != 0){  // N_K at the 27pt stencil must be 2n+2
          fprintf(stderr, "ERROR: N_K at the 27pt stencil must be 2n+2. Given:%d\n", N_K);
          exit(1);
        }
      } else{
        if ((N_K-2)%4 != 0){ // N_K at the 7pt and 3pt stencils must be 4n+2
          fprintf(stderr, "ERROR: N_K at the 7pt and 3pt stencils must be 4n+2. Given:%d\n", N_K);
          exit(1);
        }
      }
      if (((N_I-2)%GEN_KERNEL_NI_JAMS != 0) || ((N_J-2)%GEN_KERNEL_NJ_JAMS != 0)){
        fprintf(stderr, "ERROR: N_I and N_J must be unroll_size*n+2. Given: N_I=%d N_J=%d\n", N_I, N_J);
        exit(1);
      }
    }

    posix_memalign((void **)&W, ALIGNMENT, sizeof(double)*8*2);
    posix_memalign((void **)&A, ALIGNMENT, sizeof(double)*DOMAIN_SIZE+GEN_KERNEL_A_OFFSET);
    posix_memalign((void **)&R, ALIGNMENT, sizeof(double)*(DOMAIN_SIZE+GEN_KERNEL_R_OFFSET));
    R += GEN_KERNEL_R_OFFSET;
    A += GEN_KERNEL_A_OFFSET;


    for (i=0; i<DOMAIN_SIZE; i++) {
        A[i] = i; 
        R[i] = 0;
    }

    for (i=0; i<8*2; i+=2) {
        W[i] = 0.1*(i+1);
        W[i+1] = 0.1*(i+1);     // temporary solution for the duplicated weight coeffs. in the SIMD registers
    }

#if VERIFY
    double * R2;
    posix_memalign((void **)&R2, ALIGNMENT, sizeof(double)*DOMAIN_SIZE);

    for (i=0; i<DOMAIN_SIZE; i++)
        R2[i] = 0;
    kernel_reference(A,R2,W);

    kernel_wrap(A,R,W);

    // compare results
    double r1_l1=0,r1_l2=0,r2_l1=0,r2_l2=0;

    for (i=0; i<DOMAIN_SIZE; i++) {
        r1_l1 += R[i];
        r2_l1 += R2[i];
        r1_l2 += pow(R[i],2);
        r2_l2 += pow(R2[i],2);
    }
	if (mpi_rank ==0) {
        printf("******************************************************\n");
        printf("Verifying results\n");
        printf("%s:\n", GEN_KERNEL_NAME);
        printf("%9s: %20s %20s\n", "NORM", "L1", "L2");
        printf("%3f %20e\n", r1_l1, r1_l2);
        printf("%9s: \n%f %20e\n", "Reference", r2_l1, r2_l2);
        printf("******************************************************");
    }
    if (abs(r1_l1 - r2_l1)/r2_l1 > 1e-10) {
    	if (mpi_rank ==0) {
            fprintf(stderr, "BROKEN KERNEL.\nDumping results to files ...");
            print_3Darray("input_dump.txt", A);
            print_3Darray("asm_dump_results.txt", R);
            print_3Darray("c_dump_results.txt", R2);
        }
        return 1;
    }


    free(R2);
#endif

    double t_min = 1000000, t_max= -1, tpercycle;
    t=0;
    tests_remain = N_TESTS;    
    while(tests_remain--) {
      
#if FLUSH_ALL_CACHES
    flush_caches();
#elif FLUSH_L1_CACHE
    flush_l1();
#endif
#if TOUCH_L1
    touch_half_l1(A,DOMAIN_SIZE);
    touch_half_l1(R,DOMAIN_SIZE);
#endif

//      MPI_Barrier(MPI_COMM_WORLD);
      t_start = MPI_Wtime();
      for (i=0; i<N_CYCLES; i++) kernel_wrap(A,R,W);
//      MPI_Barrier(MPI_COMM_WORLD);
      t_end = MPI_Wtime();
      tpercycle = (t_end - t_start) / N_CYCLES;

      if (mpi_rank ==0) {      
#if PRINT_TIME_DETAILS
          if (tests_remain == N_TESTS-1)
              printf("\n******************************************************\n");
          printf("TEST#%02d time: %e\n",(N_TESTS-tests_remain),tpercycle);
          if (tests_remain == 0)
              printf("******************************************************");
#endif
      }
      t += tpercycle/N_TESTS;
      if (t_min > tpercycle) t_min = tpercycle;
      if (t_max < tpercycle) t_max = tpercycle;
    }

#if PRINT_NODE_INFO
    int length;
    char name[BUFSIZ];
    MPI_Get_processor_name(name, &length);
    MPI_Barrier(MPI_COMM_WORLD);
    printf("\n");
    printf("%s: Participating process %d of %d\n", name, mpi_rank, mpi_size);
    MPI_Barrier(MPI_COMM_WORLD);
#endif

	if (mpi_rank ==0) {
#if PRINT_INFO
        printf("******************************************************\n");
        printf("Actions: \n");
#if FLUSH_ALL_CACHES
        printf("L3 flushed: 1\n");
#else
        printf("L3 flushed: 0\n");
#endif        
#if FLUSH_L1_CACHE
        printf("L1 flushed: 1\n");
#else
        printf("L1 flushed: 0\n");
#endif
#if TOUCH_L1
        printf("L1 touched: 1\n");
#else
        printf("L1 touched: 0\n");
#endif    
        printf("\n");
        printf("******************************************************\n");
        printf("[%s]\n", GEN_KERNEL_NAME); 
        printf("Jams in i: %d\n", GEN_KERNEL_NI_JAMS);
        printf("Jams in j: %d\n", GEN_KERNEL_NJ_JAMS);
        printf("Number of tests    %d\n", N_TESTS);
        printf("Number of stencils %d\n", N_STENCILS);
        printf("Ni: %d Nj: %d Nk: %d \n", N_I, N_J, N_K);
        printf("Total memory allocation ~ %d B\n", sizeof(double)*DOMAIN_SIZE*2);    
        printf("total flops:       %e G \n", N_FLOPS/1e9);
        printf("total time(s)    : %e\n", t*N_TESTS);
        printf("time/test(s)     : %e\n", t);
        printf("******************************************************\n");
#endif
#if PRINT_PERFORMANCE
        printf("******************************************************\n");
//        printf("MStencil/s  THEORETICAL PEAK: %f  \n", 850.0*2.0/GEN_KERNEL_OPERATOR_SIZE);
        printf("******************************************************\n");
        printf("GFlop/s     AVG: %e  \n", N_FLOPS/(1e9*t));
        printf("MStencil/s  AVG: %f  \n", N_STENCILS/(1e6*t));
//        printf("Flop efficiency  AVG: %f  \n", (N_STENCILS/(1e6*t))/(850.0*2.0/GEN_KERNEL_OPERATOR_SIZE));
        printf("******************************************************\n");
        printf("GFlop/s     MAX: %e  \n", N_FLOPS/(1e9*t_min));
        printf("MStencil/s  MAX: %f  \n", N_STENCILS/(1e6*t_min));
//        printf("Flop efficiency  MAX: %f  \n", (N_STENCILS/(1e6*t_min))/(850.0*2.0/GEN_KERNEL_OPERATOR_SIZE));
        printf("******************************************************\n");
        printf("GFlop/s     MIN: %e  \n", N_FLOPS/(1e9*t_max));
        printf("MStencil/s  MIN: %f  \n", N_STENCILS/(1e6*t_max));
//        printf("Flop efficiency  MIN: %f  \n", (N_STENCILS/(1e6*t_max))/(850.0*2.0/GEN_KERNEL_OPERATOR_SIZE));
        printf("******************************************************\n");
#endif
//#if PRINT_EFFICIENCY
    //    printf("******************************************************\n");
    //    double cycles_per_iter = t*N_TICKS/N_STENCILS*(GEN_KERNEL_NI_JAMS*GEN_KERNEL_NJ_JAMS);
    //    printf("Load/Store Unit Efficiency      : %e  \n", GEN_KERNEL_LS_CYCLES/cycles_per_iter);
    //    printf("Floating Point Unit Efficiency  : %e  \n", GEN_KERNEL_FPU_CYCLES/cycles_per_iter);
    //    printf("******************************************************\n");
//#endif
    }
    MPI_Finalize();

    free(A-GEN_KERNEL_A_OFFSET);
    free(R-GEN_KERNEL_R_OFFSET);
    free(W);

    return 0;
}

void print_3Darray(char *filename, double *array) {
    int i,j,k;
    FILE *fp;
    if(!(fp = fopen(filename, "w")))
        printf("ERROR: cannot open file for writing\n");

    for (i=0; i<N_I; i++) {
        // new page
        fprintf(fp, "\n***************** i=%d *****************\n",i);
        for (j=0; j<N_J; j++) {
            for (k=0; k<N_K; k++) {
                fprintf(fp, "%e    ", array[(i*N_J+j)*N_K+k]);
            }
            // new line
            fprintf(fp, "\n");
        }
    }
    fclose(fp);
}
