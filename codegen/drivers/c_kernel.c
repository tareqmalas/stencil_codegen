#include "gen_kernel.ic"
#include "driver_defaults.h"

#define AA(i,j,k) (A[((i)*N_J+(j))*N_K+(k)])
#define RR(i,j,k) (R[((i)*N_J+(j))*N_K+(k)])
void kernel_reference_27pt(const double *restrict A,double *restrict R,const double *restrict W)
{
  int i,j,k;
  // Weights mapping
  //frame   k=0     k=1     k=2
  //         j       j       j
  //       7 4 7   6 2 6   7 4 7
  //     i 5 3 5   1 0 1   5 3 5
  //       7 4 7   6 2 6   7 4 7
  for (i=1; i<N_I-1; i++) {
    for (j=1; j<N_J-1; j++) {
      for (k=1; k<N_K-1; k++) {
        RR(i,j,k) =
          W[0]*(AA(i,j,k))+
          W[2]*(AA(i  ,j-1,k  ) + AA(i  ,j+1,k  ))+
          W[4]*(AA(i-1,j  ,k  ) + AA(i+1,j  ,k  ))+
          W[6]*(AA(i  ,j  ,k-1) + AA(i  ,j  ,k+1))+
          W[8]*(AA(i-1,j  ,k-1) + AA(i-1,j  ,k+1)+
                AA(i+1,j  ,k-1) + AA(i+1,j  ,k+1))+
          W[10]*(AA(i  ,j-1,k-1) + AA(i  ,j+1,k-1)+
                AA(i  ,j-1,k+1) + AA(i  ,j+1,k+1))+
          W[12]*(AA(i-1,j-1,k  ) + AA(i-1,j+1,k  )+
                AA(i+1,j-1,k  ) + AA(i+1,j+1,k  ))+
          W[14]*(AA(i-1,j-1,k-1) + AA(i-1,j+1,k-1)+
                AA(i-1,j-1,k+1) + AA(i-1,j+1,k+1)+
                AA(i+1,j-1,k-1) + AA(i+1,j+1,k-1)+
                AA(i+1,j-1,k+1) + AA(i+1,j+1,k+1));
      }
    }
  }
}

void kernel_reference_3pt(const double *restrict A,double *restrict R,const double *restrict W)
{
  int i,j,k;
  for (i=1; i<N_I-1; i++) {
    for (j=1; j<N_J-1; j++) {
      for (k=1; k<N_K-1; k++) {
        RR(i,j,k) =
          W[0]*AA(i  ,j  ,k  )+
	  W[2]*(AA(i  ,j  ,k-1)+AA(i  ,j  ,k+1));
      }
    }
  }
}

void kernel_reference_7pt(const double *restrict A,double *restrict R,const double *restrict W)
{
  int i,j,k;
  // Weights mapping
  //frame   k=0     k=1     k=2
  //         j       j       j
  //                 2          
  //     i   3     1 0 1     3  
  //                 2          
  for (i=1; i<N_I-1; i++) {
    for (j=1; j<N_J-1; j++) {
      for (k=1; k<N_K-1; k++) {
        RR(i,j,k) =
          W[0]*(AA(i,j,k))+
          W[2]*(AA(i  ,j-1,k  ) + AA(i  ,j+1,k  ))+
          W[4]*(AA(i-1,j  ,k  ) + AA(i+1,j  ,k  ))+
          W[6]*(AA(i  ,j  ,k-1) + AA(i  ,j  ,k+1));
      }
    }
  }
}

