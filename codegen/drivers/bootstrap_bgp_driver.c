#define _XOPEN_SOURCE 600  /* make stdlib.h provide posix_memalign() in strict C99 mode */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <strings.h>
#include "mpi.h"
#include "utils.h"

#define N_CYCLES (10000)

#include "gen_kernel.ic"

#define N_TICKS (850.0*1000.0*1000.0)
#define OUT_FILE "../results/bootstrap_bgp/bootstrap_raw.csv"

#ifndef PRINT_NODE_INFO
#define PRINT_NODE_INFO (1)
#endif

int main (int argc, char *argv[]) 
{
  int mpi_rank, mpi_size;
  int i, j, tests_remain,knee=0;
  double t_start, t_end, tpercycle;
  double t_test[MAX_FILL+1];
  FILE *fp = fopen(OUT_FILE,"w");

  MPI_Init(&argc,&argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size (MPI_COMM_WORLD, &mpi_size);


  STACK_INIT() // Reserves GPR registers 11 and 12 for the stack pointers
  
#if PRINT_NODE_INFO
  int length;
  char name[BUFSIZ];
  MPI_Get_processor_name(name, &length);
  MPI_Barrier(MPI_COMM_WORLD);
  printf("%s: Participating process %d of %d\n", name, mpi_rank, mpi_size);
  MPI_Barrier(MPI_COMM_WORLD);
#endif
  printf("Number of cycles: %d\n", N_CYCLES);
  printf("%s\t\t%s\t%s\n", "Kernel's name:", "time/cycle: ","cycles/iteration: ");
  fprintf(fp,"kernel_name, I_test, I_depend, hazard, fill_size, time/cycle, cycles/iteration\n");
  int kernelno;
  for(kernelno=0; kernelno<N_KERNELS; kernelno++) {
    t_start = MPI_Wtime();

    REG_SAVE()
    (*KernelList[kernelno].func)();
    REG_RESTORE()

    t_end = MPI_Wtime();
    tpercycle = (t_end - t_start) / (double) N_CYCLES;
    t_test[KernelList[kernelno].fill] = tpercycle*N_TICKS; 
    printf("%21s\t%e\t%3.2f\n", KernelList[kernelno].name, tpercycle, t_test[KernelList[kernelno].fill]);

    fprintf(fp,"%s, %s, %s, %s, %d, %e, %f\n", 
        KernelList[kernelno].name, 
        KernelList[kernelno].i_test, 
        KernelList[kernelno].i_depend, 
        KernelList[kernelno].hazard, 
        KernelList[kernelno].fill, 
        tpercycle, 
        t_test[KernelList[kernelno].fill]);

    if (KernelList[kernelno].fill == MAX_FILL) {
//      for(i=1;i<MAX_FILL+1;i++) {
//        printf("%d\t%0.00f\t%d\n", i, t_test[i]-t_test[i-1], (t_test[i] - t_test[i-1] > 0.5));
//        if ((t_test[i] - t_test[i-1] < 0.3) & (knee >-100))
//          printf("\nWARNING: multiple knees found");
//        if (t_test[i] - t_test[0] < 0.5)
//          knee = i;
//      }
//      printf("\nTest instruction latency (last drop): %d\n\n\n", knee);
//      knee = 0;      
      printf("\n");
    }
  }

  MPI_Finalize();
  fclose(fp);
  return 0;
}
