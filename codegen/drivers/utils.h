#define GPR_STACK_INIT()\
int saved_gprs_data_buf[20*4+8]; \
register int *saved_gprs_base_p asm("11");

#define FPR_STACK_INIT()\
complex double FP_stack[32]; \
register complex double *saved_fpr_base_p asm("12");

#define STACK_INIT() \
    register int sixteen asm ("13");\
    sixteen = 16;\
    GPR_STACK_INIT()\
    FPR_STACK_INIT()
    
#define REG_SAVE() \
    FPR_SAVE() \
    GP_REG_SAVE()

#define REG_RESTORE() \
    GP_REG_RESTORE() \
    FPR_RESTORE() 


#define FPR_SAVE() \
{ \
saved_fpr_base_p = FP_stack-1; \
    asm volatile ("stfpdux 13, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 14, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 15, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 16, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 17, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 18, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 19, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 20, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 21, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 22, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 23, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 24, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 25, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 26, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 27, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 28, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 29, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 30, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("stfpdux 31, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));}

#define FPR_RESTORE() \
    { \
    saved_fpr_base_p = FP_stack-1; \
    asm volatile ("lfpdux 13, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 14, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 15, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 16, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 17, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 18, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 19, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 20, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 21, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 22, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 23, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 24, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 25, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 26, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 27, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 28, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 29, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 30, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));\
    asm volatile ("lfpdux 31, %0, %1":"+b" (saved_fpr_base_p):"r" (sixteen));}

#define GP_REG_SAVE() \
{ saved_gprs_base_p = saved_gprs_data_buf -1; \
asm volatile ("stwu 13, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 14, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 15, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 16, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 17, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 18, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 19, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 20, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 21, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 22, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 23, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 24, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 25, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 26, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 27, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 28, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 29, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("stwu 30, 4(%0)" : "+b"(saved_gprs_base_p)); }

//asm volatile ("stwu 31, 4(%0)" : "+b"(saved_gprs_base_p))

#define GP_REG_RESTORE() \
{ saved_gprs_base_p = saved_gprs_data_buf - 1; \
asm volatile ("lwzu 13, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 14, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 15, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 16, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 17, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 18, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 19, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 20, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 21, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 22, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 23, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 24, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 25, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 26, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 27, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 28, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 29, 4(%0)" : "+b"(saved_gprs_base_p)); \
asm volatile ("lwzu 30, 4(%0)" : "+b"(saved_gprs_base_p)); }

