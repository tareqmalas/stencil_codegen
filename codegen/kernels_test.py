#!/usr/bin/env python3

# Module to test the printing of the kernels

from kernels.bagm_mute2x2_7pt import Generator

def main():    
    
    gen_params = {'interleave':"1", 'verbose':"1"}
    g = Generator(**gen_params)
    
    print(g.gen_initialize_registers())
    print(g.gen_prologue())
    print(g.gen_inner_iter())
    print(g.gen_epilogue())

if __name__ == "__main__":
    main()