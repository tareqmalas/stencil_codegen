#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) 
{
    /*
     R[1] = 4*10+5*20+6*30 = 40+100+180  = 320
     R[2] = 5*10+6*20+7*30 = 50+120+210 = 380
    */
    double A[4]  = {4.0, 5.0, 6.0, 7.0};
    double W[3]  = {10.0, 20.0, 30.0};
    double R[4]  = {0.0, 0.0, 0.0, 0.0};

    printf("Initial\n");    
    printf("A: %e   %e   %e   %e\n", A[0], A[1], A[2], A[3]);
    printf("W: %e   %e   %e\n", W[0], W[1], W[2]);
    printf("R: %e   %e   %e   %e\n", R[0], R[1], R[2], R[3]);
    printf("\n");
    
    register double *a asm ("6");  
    register double *r asm ("7"); 
    register double *w asm ("8");
    register int zero asm ("17");
    register int eight asm ("18"); 
    register int sixteen asm ("19"); 
    zero = 0;
    eight = 8;
    sixteen = 16;

/*    
    a ← A[i,j,k+0:k+1]  ;mute a01
    r21 = a01 '* W0  
    a01.p ← A[i,j,k+2]  ;mute a21
    r21 += a21 * W1
    a21.s ← A[i,j,k+3]  ;mute a23 
    r21 += a23 '* W2
    r21 → R[i,j,k+1:k+2]   
    ---------------------------------------
    r43 = a23 '* W0
    a23.p ← A[i,j,k+4]  ;mute a43
    r43 += a43 * W1
    a43.s ← A[i,j,k+5]  ;mutes a45
    r43 += a45 '* W2
    r43 → R[i,j,k+3:k+4]
    r65 = a45 '* W0
*/
        
    a = (double *) A-2;
    r = (double *) R-1;
    w = (double *) W-1;
/*
    Registers usage:
    20  w0
    21  w1
    22  w2
    0   input data
    1   results 1/2
    2   results 2/2
*/
    asm(".__load_in:");
    // load replicated values of the three weight in 3 SIMD-registers
    // load primary
    asm volatile("lfdux 20, %0, %1":"+b" (w):"b" (eight));
    asm volatile("lfdux 21, %0, %1":"+b" (w):"b" (eight));
    asm volatile("lfdux 22, %0, %1":"+b" (w):"b" (eight));
    // load secondary
    w = (double *) W-1;
    asm volatile("lfsdux 20, %0, %1":"+b" (w):"b" (eight));
    asm volatile("lfsdux 21, %0, %1":"+b" (w):"b" (eight));
    asm volatile("lfsdux 22, %0, %1":"+b" (w):"b" (eight));

    // load a01
    asm volatile("lfpdux 0, %0, %1":"+b" (a):"b" (sixteen));
    // prepare pointer for FP load (8 bytes)
    a++;
    
    // r21 = a01 '* W0
    asm volatile("fxmul 1, 0, 20");
    // a01.p ← A[k+2]  ;mute a21
    asm volatile("lfdux 0, %0, %1":"+b" (a):"b" (eight));
    // r21 += a21 '* W1
    asm volatile("fxcpmadd 1, 21, 0, 1");
    // a21.s ← A[k+3]  ;mute a23
    asm volatile("lfsdux 0, %0, %1":"+b" (a):"b" (eight));
    // r21 += a23 '*W2
    asm volatile("fxcxma 1, 22, 0, 1");
    // r21 → R[k+1:k+2]   
    asm volatile("stfxdux 1, %0, %1":"+b" (r):"b" (sixteen));
    
    printf("final values\n");    
    printf("A: %e   %e   %e   %e\n", A[0], A[1], A[2], A[3]);
    printf("W: %e   %e   %e\n", W[0], W[1], W[2]);
    printf("R: %e   %e   %e   %e\n", R[0], R[1], R[2], R[3]);
    printf("Rc:%e   %e   %e   %e",0.0, 
        A[0]*W[0]+A[1]*W[1]+A[2]*W[2], 
        A[1]*W[0]+A[2]*W[1]+A[3]*W[2],0.0);
    
    return 0;
}
